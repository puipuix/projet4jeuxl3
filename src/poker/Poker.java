package poker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Quentin Colras
 */
public class Poker {

	/**
	 * @param paquet Liste de carte repr�sentant un paquet de 52 cartes
	 * @param table Liste de carte repr�sentant les cartes sur la table
	 * @param currentJoueur Le joueur qui joue ce tour ci
	 * @param listeJoueur Repr�sente la liste de tous les joueurs
	 * @param listeJoueurActif Repr�sente la liste de tous les joueurs actifs
	 * @param miseTable La mise sur la table
	 * @param tour Repr�sente le joueur actuel, exemple tour = 0 si c'est le joueur 1 qui joue
	 * @param manche Compteur de manche
	 * @param valeurAsuivre Valeur a suivre par les joueurs, par d�faut = 20
	 * @param check D�finit si un joueur peut check ou non
	 * @param tourCheck Indice m�morisant l'indice du joueur ayant check en premier
	 * @param nbCoupJouer Repr�sente le nombre de coup jouer dans la manche
	 * @param passerManche Valeur booleenne permettant de savoir si on passe la manche ou non
	 */
	List<Carte> paquet;
	List<Carte> table;
	JoueurPoker currentJoueur;
	List<JoueurPoker> listeJoueur;
	List<JoueurPoker> listeJoueurActif;
	int miseTable;
	int tour;
	int manche;
	int valeurAsuivre;
	boolean check;
	int tourCheck;
	int nbCoupJouer;
	boolean passerManche;

	/**
	 * Constructeur permettant de cr�er une partie
	 * @param nbJoueur nombre de joueurs dans la partie
	 */
	public Poker(int nbJoueur) {
		manche = 0;
		tour = 0;
		listeJoueur = new ArrayList<JoueurPoker>();
		listeJoueurActif = new ArrayList<JoueurPoker>();
		for (int i = 0; i < nbJoueur; i++) {
			JoueurPoker j = new JoueurPoker(i + 1);
			if (i == 0)
				currentJoueur = j;
			listeJoueur.add(j);
			listeJoueurActif.add(j);
		}
		reset(nbJoueur);
	}

	/**
	 * Permet de reset la partie lorsqu'un joueur a gagner un round
	 * @param nbJoueur nombre de joueurs dans la partie
	 */
	public void reset(int nbJoueur) {

		passerManche = false;
		paquet = new ArrayList<Carte>();
		table = new ArrayList<Carte>();
		nbCoupJouer = 0;
		miseTable = 0;
		valeurAsuivre = 20;
		check = false;
		tourCheck = -1;
		initialisation();
		for (int i = 0; i < nbJoueur; i++) {
			JoueurPoker j = listeJoueur.get(i);
			j.getMain().clear();
			j.getMainTable().clear();
			j.setCouche(false);
			j.setStatus(false);
			ajouteCarteMainJoueur(j, 2);
		}
		currentJoueur = listeJoueur.get(tour);
	}

	/**
	 * Affiche l'�tat de la partie, fonction permettant au d�bug
	 */
	public void etatPartie() {

		System.out.print("\n\nMise sur la table : " + miseTable);
		System.out.print(" | Tour : " + tour);
		System.out.print(" | TourCheck : " + tourCheck);
		System.out.print(" | Manche : " + manche);
		System.out.print(" | Check : " + check);
		System.out.print("\nValeur a Suivre : " + valeurAsuivre);
		System.out.println(" | Carte sur la table : " + table);
		for (int i = 0; i < listeJoueur.size(); i++) {
			System.out.print("\n" + listeJoueur.get(i).getNom());
			JoueurPoker j = listeJoueur.get(i);
			System.out.print(" Couche :" + j.getCouche());
			System.out.print(" Check :" + j.getStatus());
			System.out.print(" Argent :" + j.getArgent());
			System.out.print(" Main du joueur : " + j.getMain());
		}
	}

	/**
	 * Initialise la partie
	 */
	public void initialisation() {
		remplirPaquet();
		melangePaquet();
		addCarteTable(3);
		for (int i = 0; i < listeJoueur.size(); i++) {
			listeJoueur.get(i).clearMain();
			ajouteCarteMainJoueur(listeJoueur.get(i), 2);
		}
	}

	/**
	 * Cr�er la main table du joueur
	 * @param j Joueur a dont on doit cr�er la main table
	 */
	public void creationMainTableJoueur(JoueurPoker j) {
		j.copyMain(j.getMain());
		j.copyMain(table);
	}

	/**
	 * Repr�sente le nombre de joueurs coucher
	 */
	public int nombreJoueurCoucher() {
		int s = 0;
		for (int i = 0; i < listeJoueur.size(); i++) {
			if (listeJoueur.get(i).getCouche())
				s++;
		}
		return s;
	}

	/**
	 * Nombre de joueurs n'ayant plus d'argent
	 */
	public int nombreJoueurSansSous() {
		int s = 0;
		for (int i = 0; i < listeJoueur.size(); i++) {
			if (listeJoueur.get(i).getArgent() == 0)
				s++;
		}
		return s;
	}

	/**
	 * Test afin de savoir si il reste une personne non coucher
	 */
	public int testSurvivant() {
		int somme = 0, indiceSurvivant = -1;
		for (int i = 0; i < listeJoueur.size(); i++) {
			if (!listeJoueur.get(i).getCouche()) {
				indiceSurvivant = i;
				somme++;
			}
		}
		if (somme == 1) {
			victoireJoueur(indiceSurvivant);
			return 1;
		}
		return -1;
	}

	/**
	 * Test permettant de savoir si un joueur a gagner, si un seul joueur gagne alors il remporte la totalit� des gains, sinon les deux joueurs se partagent la mise
	 */
	public boolean testVictoire() {
		List<Integer> listePretendant = new ArrayList<Integer>();
		for (int i = 0; i < listeJoueur.size(); i++) {
			if (!listeJoueur.get(i).getCouche()) {
				creationMainTableJoueur(listeJoueur.get(i));
				listePretendant.add(i);
			}
		}
		if (listePretendant.size() == 1) {} 
		else {
			int win = 0;
			int win2 = -1;
			for (int i = 1; i < listePretendant.size(); i++) {
				JoueurPoker a = listeJoueur.get(listePretendant.get(win));
				JoueurPoker b = listeJoueur.get(listePretendant.get(i));
				int result = comparePuissance(a, b);
				if (result == -1) {
					win2 = -1;
				} else if (result == 0) {
					win2 = i;
				} else {
					win = i;
					win2 = -1;
				}
			}
			if (win2 != -1)
				victoireJoueur(win, win2);
			else
				victoireJoueur(win);
		}
		int vivant = 0;
		for (int i = 0; i < listeJoueur.size() && vivant < 2; i++) {
			if (listeJoueur.get(i).getArgent() > 0) {
				vivant++;
			}
		}
		return vivant < 2;
	}

	/**
	 * Fonction qui donne l'argent de la table au joueur gagnant
	 * @param j1 Joueur ayant gagner le round
	 */
	public void victoireJoueur(int j1) {
		JoueurPoker j = listeJoueur.get(j1);
		j.setArgent(j.getArgent() + miseTable);
		miseTable = 0;
	}

	/**
	 * Fonction qui partage l'argent de la table au 2 joueurs gagnants
	 * @param j1 Premier joueur ayant gagner le round
	 * @param j2 Deuxi�me joueur ayant gagner le round
	 */
	public void victoireJoueur(int j1, int j2) {
		int moitie = miseTable / 2;
		JoueurPoker j = listeJoueur.get(j1);
		j.setArgent(j.getArgent() + moitie);
		j = listeJoueur.get(j2);
		j.setArgent(j.getArgent() + moitie);
		miseTable = 0;
	}

	/**
	 * Reset les status des joueurs de la partie
	 */
	public void resetStatus() {
		for (int i = 0; i < listeJoueur.size(); i++)
			listeJoueur.get(i).setStatus(false);
	}

	/**
	 * Permet de faire la rotation des joueurs
	 */
	public boolean rotateJoueur() {

		int taille = listeJoueur.size();
		boolean rep = false;
		int tentative = 0;
		while (!rep && tentative < taille) {
			if (listeJoueur.get(tour % (taille)).getCouche() == true
					|| listeJoueur.get(tour % (taille)).getArgent() == 0) {
				tour++;
				tentative++;
			} else {
				rep = true;
			}
		}
		if (rep) {
			currentJoueur = listeJoueur.get(tour % (taille));
		}
		return rep;
	}

	/**
	 * Fais coucher un joueur
	 * @param j Joueur qui se couche
	 */
	public void seCouche(JoueurPoker j) {
		listeJoueurActif.remove(j);
		j.setCouche(true);
	}

	/**
	 * Set le tour au bon joueur
	 */
	public void setBonTour() {
		tour = listeJoueur.indexOf(currentJoueur);
	}

	/**
	 * Retire de la partie le joueur n'ayant plus d'argent et n'ayant pas gagner le round
	 */
	public void retireJoueurSansSous() {
		int i = 0;
		while (i < listeJoueur.size()) {
			if (listeJoueur.get(i).getArgent() == 0)
				listeJoueur.remove(i);
			else
				i++;
		}
	}

	/**
	 * D�finit si un joueur peut check ou non
	 * @param joueur Joueur dont on test si il peut check ou non
	 */
	public boolean peutCheck(JoueurPoker joueur) {
		int j = listeJoueur.indexOf(joueur);
		for (int i = 0; i < j; i++)
			if (!listeJoueur.get(i).getStatus()) {
				tour = tourCheck;
				check = false;
				return false;
			}
		return true;
	}

	/**
	 * Fait check un joueur
	 * @param j Joueur qui check
	 */
	public void check(JoueurPoker j) {
		j.setStatus(true);
		if (tourCheck == -1)
			tourCheck = tour;
		check = true;
	}

	/**
	 * Retour en arriere si un d�s de la manche n'a pas check
	 */
	public void back() {
		tour = tourCheck;
		currentJoueur = listeJoueur.get(tour);
		tourCheck = -1;
		resetStatus();
	}

	/**
	 * D�finit la valeur a miser par le joueur
	 * @param j Joueur dont on fait miser
	 * @param valeur La valeur a suivre, donc pour miser il faut mettre plus que cette valeur
	 * @return
	 */
	public boolean miser(JoueurPoker j, int valeur) {
		if (valeur < valeurAsuivre)
			return false;
		else {
			valeurAsuivre = j.miser(valeur);
			miseTable += valeurAsuivre;
			return true;
		}
	}

	/**
	 * Fait tapis un joueur
	 * @param j Joueur qui tapis
	 */
	public void tapis(JoueurPoker j) {
		valeurAsuivre = j.tapis();
		miseTable += valeurAsuivre;
	}

	
	/**
	 * Fait suivre un joueur
	 * @param j Joueur qui suit
	 */
	public void suivre(JoueurPoker j) {
		miseTable += j.suivre(valeurAsuivre);
	}

	/**
	 * Calcul la puissance d'une main
	 * @param main main dont on veut calculer la puissance
	 */
	public double calculPuissance(List<Carte> main) {
		int[][] tabOcc = trieListe(main);
		int[][] tabCol = creerTabCol(main);
		double puissance = 0;
		int quinte = testQuinte(main);
		if (quinte != -1) {
			if (quinte > 14) {
				quinte /= 2;
				if (quinte == 14) {
					puissance = 4000;
				} else {
					puissance = 3000 + quinte;
				}
			} else {
				puissance = 400 + quinte;
			}
		}
		else {
			if (testCouleur(tabCol[1])) {
				puissance = 500;
			} else { 
				int a = tabOcc[0][0], b = tabOcc[1][0], c = tabOcc[1][1];
				if (b == 4) {
					puissance = 2000 + a;
				} else if (b == 3) {
					if (c == 2) {
						puissance = 700 + a;
					} else {
						puissance = 300 + a;
					}
				} else if (b == 2) {
					if (c == 2) {
						puissance = 200 + a;
						double p = (double) (tabOcc[0][1]) / 10;
						puissance += p;
					} else {
						puissance = 100 + a;
					}
				} else if (b == 1) {
					puissance = a;
				}
			}
		}
		return puissance;
	}

	/**
	 * Trie la main
	 * @param main Main que l'on veut trier
	 */
	public int[][] trieListe(List<Carte> main) {
		Collections.sort(main);
		Collections.reverse(main);
		int[][] tabOcc = creerTabOcc(main);
		triInsertion(tabOcc);
		return tabOcc;
	}

	public void triInsertion(int[][] tab) {
		int j, x, x2;
		for (int i = 1; i < tab[1].length; i++) {
			x = tab[1][i];
			x2 = tab[0][i];
			j = i;
			while (j > 0 && tab[1][j - 1] < x) {
				inverseColonne(tab, j - 1, j);
				j--;
			}
			tab[1][j] = x;
			tab[0][j] = x2;
		}
	}

	/**
	 * Inverse deux colonnes d'un tableau
	 * @param tab Tableau dont on veut inverser les colonnes
	 * @param src 1er colonne
	 * @param dst 2e colonne
	 */
	public void inverseColonne(int[][] tab, int src, int dst) {
		int a = tab[0][src], b = tab[1][src];
		tab[0][src] = tab[0][dst];
		tab[1][src] = tab[1][dst];
		tab[0][dst] = a;
		tab[1][dst] = b;
	}

	/**
	 * Compare la puissance des mains en deux joueurs
	 * @param j1 Joueur 1
	 * @param j2 Joueur 2
	 */
	public int comparePuissance(JoueurPoker j1, JoueurPoker j2) {
		double pM1 = calculPuissance(j1.getMainTable());
		double pM2 = calculPuissance(j2.getMainTable());
		double puissanceTable = calculPuissance(table);
		if (pM1 == pM2 && pM1 == puissanceTable) {
			double puissanceMain1 = calculPuissance(j1.getMain());
			double puissanceMain2 = calculPuissance(j2.getMain());
			if (puissanceMain1 > puissanceMain2)
				return -1;
			else if (puissanceMain1 < puissanceMain2)
				return 1;
			else
				return 0;
		} else {
			if (pM1 > pM2)
				return -1;
			else if (pM1 < pM2)
				return 1;
			else
				return 0;
		}
	}

	/**
	 * Test si on a pas une couleur
	 */
	public boolean testCouleur(int[] tab) {
		int a = 0, b = 0, c = 0, d = 0;
		for (int i = 0; i < tab.length; i++) {
			if (tab[i] == 1)
				a++;
			else if (tab[i] == 2)
				b++;
			else if (tab[i] == 3)
				c++;
			else if (tab[i] == 4)
				d++;
		}
		if (a == 5 || b == 5 || c == 5 || d == 5)
			return true;
		return false;
	}

	/**
	 * Ajoute des cartes dans la main d'un joueur
	 * @param j Joueur dont on veut ajouter des cartes 
	 * @param nb Nombre de cartes � ajouter
	 */
	public void ajouteCarteMainJoueur(JoueurPoker j, int nb) {
		for (int i = 0; i < nb; i++) {
			j.addCarte(paquet.get(0));
			paquet.remove(0);
		}
	}

	/**
	 * Test si la main est une quinte ou non
	 * @param main Liste de cartes � tester
	 */
	public int testQuinte(List<Carte> main) {
		int cpt = 0, cpt2 = 0, i = 1, a, b, c, d, result = -1;
		while (i < main.size()) {
			a = main.get(i - 1).getValeur();
			b = main.get(i).getValeur();
			c = main.get(i - 1).getNom();
			d = main.get(i).getNom();
			if (a == b + 1) {
				if (c == d)
					cpt2++;
				else
					cpt2 = 0;
				if (result == -1)
					result = a;
				cpt++;
			} else {
				cpt = 0;
			}
			if (cpt >= 4) {
				if (cpt2 >= 4)
					result += result; // Quinte flush
				// Quinte
			}
			i++;
		}
		if (cpt < 4)
			result = -1;
		return result;
	}

	/**
	 * Tableau mettant en relation la carte et sa couleur, permettant le calcul de la puissance
	 * @param main Main
	 * @return
	 */
	public int[][] creerTabCol(List<Carte> main) {
		int[][] tab = new int[2][7];
		for (int i = 0; i < main.size(); i++) {
			tab[0][i] = main.get(i).getValeur();
			tab[1][i] = main.get(i).getNom();
		}
		return tab;
	}

	/** 
	 * Tableau comptant le nombre d'occurence, permettant le calcul de la puissance
	 * @param main
	 * @return
	 */
	public int[][] creerTabOcc(List<Carte> main) {
		int[][] tab = new int[2][7];
		tab[0][0] = main.get(0).getValeur();
		tab[1][0] = 1;
		int compteur = 1;
		for (int i = 1; i < main.size(); i++) {
			int v = main.get(i).getValeur();
			int indice = indiceTab(tab[0], v);
			if (indice >= 0)
				tab[1][indice]++;
			else {
				tab[0][compteur] = main.get(i).getValeur();
				tab[1][compteur] = 1;
				compteur++;
			}
		}
		return tab;
	}

	public int indiceTab(int[] t, int v) {
		for (int i = 0; i < t.length; i++)
			if (t[i] == v)
				return i;
		return -1;
	}

	public void afficheTab(int[] tab) {
		System.out.println();
		for (int i = 0; i < tab.length; i++) {
			System.out.print(tab[i] + "|");
		}
		System.out.println();
	}

	public void afficheMatrice(int[][] tab) {
		System.out.println();
		for (int i = 0; i < tab.length; i++) {
			for (int j = 0; j < tab[0].length; j++) {
				System.out.print(tab[i][j] + "|");
			}
			System.out.println();
		}
	}
	

	/**
	 * Remplis le paquet de cartes
	 */
	public void remplirPaquet() {
		for (int i = 1; i <= 4; i++)
			for (int j = 2; j <= 14; j++) {
				Carte c = new Carte(i, j);
				paquet.add(c);
			}
	}

	/**
	 * M�lange le paquet
	 */
	public void melangePaquet() {
		Collections.shuffle(paquet);
	}

	public void affichagePaquet() {
		for (int i = 1; i <= paquet.size(); i++) {
			System.out.print(paquet.get(i - 1));
			if (i % 6 == 0)
				System.out.println();
		}
	}

	public int getNombreCartePaquet() {
		return paquet.size();
	}

	/**
	 * Ajoute des cartes � la table
	 * @param nb Nombre de carte � ajouter � la table
	 */
	public void addCarteTable(int nb) {
		for (int i = 0; i < nb; i++) {
			table.add(paquet.get(0));
			paquet.remove(0);
		}
	}

	public void afficheTable() {
		System.out.print("Table :");
		for (int i = 0; i < table.size(); i++)
			System.out.print(table.get(i));
	}

	public List<JoueurPoker> getListeJoueur() {
		return listeJoueur;
	}

	public List<JoueurPoker> getListeJoueurActif() {
		return listeJoueurActif;
	}

	public JoueurPoker getCurrentJoueur() {
		return currentJoueur;
	}

	public List<Carte> getTable() {
		return table;
	}

	public int getMiseTable() {
		return miseTable;
	}

	public int getTour() {
		return tour;
	}

	public void setTour(int _tour) {
		tour = _tour;
	}

	public int getValeurAsuivre() {
		return valeurAsuivre;
	}

	public void setValeurAsuivre(int _valeurAsuivre) {
		valeurAsuivre = _valeurAsuivre;
	}

	public void setManche(int _manche) {
		manche = _manche;
	}

	public int getManche() {
		return manche;
	}

	public boolean getCheck() {
		return check;
	}

	public void setCheck(boolean _check) {
		check = _check;
	}

	public int getTourCheck() {
		return tourCheck;
	}

	public void setTourCheck(int _tourCheck) {
		tourCheck = _tourCheck;
	}

	public void setNbCoupJouer(int _nbCoupJouer) {
		nbCoupJouer = _nbCoupJouer;
	}

	public int getNbCoupJouer() {
		return nbCoupJouer;
	}

	public void setPasserManche(boolean _passerManche) {
		passerManche = _passerManche;
	}

	public boolean getPasserManche() {
		return passerManche;
	}

}
