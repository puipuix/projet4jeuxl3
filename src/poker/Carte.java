package poker;

import projet4jeuxl3.ContentManager;

/**
 * @author Quentin Colras
 */
public class Carte implements Comparable <Carte>{
	
	/**
	 * @param nom Nom de la carte, 1 = Carreau, 2 = Coeur, 3 = Pique, 4 = Tr�fle
	 * @param valeur Valeur de la carte allant de 2 � 14(as)
	 */
	protected int nom;
	protected int valeur;
	
	/**
	 * Constructeur d'une carte
	 * @param _nom Nom de la carte
	 * @param _valeur Valeur de la carte
	 */
	public Carte(int _nom , int _valeur) {
		nom = _nom ; valeur = _valeur;
	}
	
	public int getNom() {
		return nom;
	}
	
	public int getValeur() {
		return valeur;
	}
	
	/**
	 * Compare la valeur d'une carte avec une autre
	 * @param c2 Carte � comparer avec la carte actuel
	 */
	@Override
	public int compareTo(Carte c2) {
		if (this.valeur < c2.valeur) return -1;
		if (this.valeur == c2.valeur) return 0;
		return 1;
	}
	
	/**
	 * Renvoie le nom r�el de la carte
	 * @param nom entier � envoyer allant de 1 � 4
	 */
	public String getTrueName(int nom) {
		if(nom == 1) return "carreau";
		if(nom == 2) return "coeur";
		if(nom == 3) return "pique";
		else return "treffle";
	}
	
	@Override
	public String toString() {
		StringBuffer sc = new StringBuffer();
		sc.append(" ").append(getTrueName(nom)).append("|").append(valeur);
		return sc.toString();
	}
	
	
	//Retourne la chaine permettant de l'afficher
	public String getName() {
		StringBuffer sc = new StringBuffer();
		sc.append(ContentManager.FILE_PO).append('/').append(valeur).append('_').append(this.getTrueName(getNom())).append(".png");
		return sc.toString();
		}

}
