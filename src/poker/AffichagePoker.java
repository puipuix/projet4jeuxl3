package poker;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import projet4jeuxl3.ContentManager;
import projet4jeuxl3.Main;
import projet4jeuxl3.MultipleRoot;
import projet4jeuxl3.PauseMenuJeux;
import projet4jeuxl3.PauseMenuVictoireDefaite;
import projet4jeuxl3.Score;

public class AffichagePoker extends Stage {

	static final double HEIGHT_CARD = 100;

	private Label mise;
	private Label valASuivre;
	private Poker jeux;
	private GridPane action;
	private HBox carteTable;
	private GridPane grille;
	private StackPane table;
	private PauseMenuJeux pmj;
	private PauseMenuVictoireDefaite pmvd;

	public AffichagePoker() {

		// Creation du primary stage
		this.setTitle("Poker");

		// Il d�finit les espacement

		jeux = new Poker(4); // Initialise la partie

		pmj = new PauseMenuJeux(this);
		pmvd = new PauseMenuVictoireDefaite(this, () -> {
			return new AffichagePoker();
		});

		grille = new GridPane();
		grille.setBackground(new Background(new BackgroundFill(Color.grayRgb(50), null, null)));
		grille.setAlignment(Pos.CENTER);

		MultipleRoot multRoot = new MultipleRoot(pmvd, pmj);
		multRoot.setMainRoot(grille);
		multRoot.resizeMainRoot(false);

		carteTable = new HBox(10);
		carteTable.setAlignment(Pos.CENTER);
		carteTable.setBackground(Background.EMPTY);
		carteTable.setMaxSize(0, 0); // Will use min size he could take
		carteTable.setBackground(new Background(new BackgroundFill(Color.web("#3A9A3A"), null, null)));
		carteTable.setPadding(new Insets(10));

		Rectangle rect = new Rectangle();
		rect.setFill(Color.rgb(15, 65, 8));
		double h = HEIGHT_CARD * 2 + 100;
		double w = HEIGHT_CARD * 7;
		rect.setHeight(h);
		rect.setWidth(w);
		rect.setArcHeight(h);
		rect.setArcWidth(w);

		mise = new Label();
		mise.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
		mise.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 20));
		mise.setTextFill(Color.WHITE);

		valASuivre = new Label();
		valASuivre.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
		valASuivre.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 15));
		valASuivre.setTextFill(Color.WHITE);

		VBox labels = new VBox(mise, valASuivre);
		labels.setMaxSize(0, 0);
		labels.setAlignment(Pos.CENTER);
		labels.setBackground(new Background(new BackgroundFill(Color.DARKRED, null, null)));
		labels.setPadding(new Insets(10));

		updateMise(jeux);
		HBox htable = new HBox(carteTable, labels);
		htable.setAlignment(Pos.CENTER);

		table = new StackPane(rect, htable); // Cr�er la table au milieu
		table.setMaxSize(0, 0);

		action = new GridPane();
		creerBoutonAction();
		action.setAlignment(Pos.CENTER_LEFT);
		action.setPadding(new Insets(5));

		affichageDepart();

		Scene sc = new Scene(multRoot, Color.CHOCOLATE);
		sc.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ESCAPE) {
				pmj.Show();
			}
		});

		// Redimentionne le gridPane si on change la taille de la fenetre
		// multRoot.resizeMainRoot(true); n'est pas appel� avant car tout les �l�ments
		// n'ont pas encore pris leurs taille ce qui pose des probl�mes
		ChangeListener<Number> resize = new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (observable.equals(heightProperty())) {
					setMinHeight(getHeight());
					heightProperty().removeListener(this);
				} else {
					setMinWidth(getWidth());
					widthProperty().removeListener(this);
				}
				multRoot.resizeMainRoot(true);
			}
		};
		this.heightProperty().addListener(resize);
		this.widthProperty().addListener(resize);
		this.setScene(sc);
		this.centerOnScreen();
	}

	private void updateMise(Poker jeux) {
		mise.setText("Table: " + jeux.getMiseTable() + "$");
		valASuivre.setText("Suivre: " + jeux.getValeurAsuivre() + "$");
	}

	private Button creerBoutonSeCouche() {
		Button couche = new Button();
		GridPane.setMargin(couche, new Insets(5));
		GridPane.setHalignment(couche, HPos.CENTER);
		GridPane.setValignment(couche, VPos.CENTER);
		couche.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 15));
		couche.setText("Se coucher");
		//couche.setAlignment(Pos.CENTER);
		couche.setOnAction(e -> {
			jeux.seCouche(jeux.getCurrentJoueur());
			if (jeux.testSurvivant() == 1) {
				jeux.setPasserManche(true);
				creerBoutonAction();
			}
			testAjoutCarte(1);
		});
		return couche;
	}

	/**
	 * Cr�er un bouton permettant de miser une somme , si la somme est inf�rieur
	 * � la valeur a suivre alors il ne se passe rien Sinon le joueur mise la
	 * somme et la valeur a suivre est actualiser avec cette mise
	 */
	private Button creerBoutonMiser() {
		Button miser = new Button();
		GridPane.setMargin(miser, new Insets(5));
		GridPane.setHalignment(miser, HPos.CENTER);
		GridPane.setValignment(miser, VPos.CENTER);
		miser.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 15));
		miser.setPadding(new Insets(5));
		miser.setText("Miser");
		miser.setAlignment(Pos.CENTER);
		miser.setOnAction(e -> {
			HBox mise = new HBox();
			TextField m1 = new TextField();
			m1.setTextFormatter(new TextFormatter<>(f -> {
				if (f.getText().matches("[0-9]*")) {
					return f;
				}

				return null;
			}));
			m1.setMinSize(TextField.USE_PREF_SIZE, TextField.USE_PREF_SIZE);
			Button valider = new Button();
			valider.setText("Valider");
			valider.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
			valider.setOnAction(e2 -> {
				if (m1.getText().length() > 0) {
					int a = Integer.parseInt(m1.getText());
					if (jeux.miser(jeux.getListeJoueur().get(jeux.getTour()), a)) {
						jeux.setCheck(true);
						creerBoutonAction();
						testAjoutCarte(1);
						updateMise(jeux);
					}
				}

				action.getChildren().remove(mise);
			});
			mise.setAlignment(Pos.CENTER);
			mise.setMaxSize(0, 0);
			mise.getChildren().add(m1);
			mise.getChildren().add(valider);
			action.add(mise, 1, 0);
		});
		return miser;
	}

	// Cr�er un bouton permettant de suivre la valeur a suivre par d�fault : 20
	private Button creerBoutonSuivre() {
		Button suivre = new Button();
		suivre.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 15));
		GridPane.setMargin(suivre, new Insets(5));
		GridPane.setHalignment(suivre, HPos.CENTER);
		GridPane.setValignment(suivre, VPos.CENTER);
		suivre.setText("Suivre");
		suivre.setAlignment(Pos.CENTER);
		suivre.setOnAction(e -> {
			if (jeux.getCheck() && jeux.getTourCheck() != -1) {
				jeux.back();
				affichageListeJoueur(false);
				creerBoutonAction();
			} else {
				jeux.suivre(jeux.getCurrentJoueur());
				updateMise(jeux);
				jeux.setCheck(true);
				testAjoutCarte(1);
				creerBoutonAction();
			}
		});
		return suivre;
	}

	private Button creerBoutonTapis() {
		Button tapis = new Button();
		tapis.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 15));
		GridPane.setMargin(tapis, new Insets(5));
		GridPane.setHalignment(tapis, HPos.CENTER);
		GridPane.setValignment(tapis, VPos.CENTER);
		tapis.setText("Tapis");
		tapis.setAlignment(Pos.CENTER);
		tapis.setOnAction(e -> {
			jeux.tapis(jeux.getListeJoueur().get(jeux.getTour()));
			jeux.setCheck(true);
			creerBoutonAction();
			updateMise(jeux);
			testAjoutCarte(1);
		});
		return tapis;
	}

	private Button creerBoutonCheck() {
		Button check = new Button();
		check.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 15));
		check.setText("Check");
		GridPane.setMargin(check, new Insets(5));
		GridPane.setHalignment(check, HPos.CENTER);
		GridPane.setValignment(check, VPos.CENTER);
		check.setOnAction(e -> {
			jeux.check(jeux.getListeJoueur().get(jeux.getTour()));
			testAjoutCarte(1);
		});
		return check;
	}

	private Button creerBoutonPause() {
		Button pause = new Button();
		pause.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 15));
		pause.setText("Pause");
		GridPane.setMargin(pause, new Insets(5));
		GridPane.setHalignment(pause, HPos.CENTER);
		GridPane.setValignment(pause, VPos.CENTER);
		pause.setOnAction(e -> {
			pmj.Show();
		});
		return pause;
	}

	private void creerBoutonAction() {
		action.getChildren().clear();
		// System.out.println("TEST PASSER MANCHE "+passerManche);
		if (jeux.getPasserManche()) {
			Button passeManche = new Button();
			passeManche.setText("Passer la manche");
			passeManche.setAlignment(Pos.CENTER);
			passeManche.setOnAction(e -> {

				jeux.setManche(jeux.getManche() + 1);
				jeux.setTour(jeux.getManche() % jeux.getListeJoueur().size());
				jeux.reset(jeux.getListeJoueur().size());
				jeux.retireJoueurSansSous();
				affichageDepart();
				updateMise(jeux);
				jeux.setPasserManche(false);
				creerBoutonAction();
			});
			action.getChildren().add(passeManche);
		} else {
			// System.out.println("TEST ELSE");
			Button passer = new Button();
			passer.setText("Test main joueur");
			passer.setAlignment(Pos.CENTER);
			passer.setOnAction(e -> {
				affichageListeJoueur(true);
				jeux.testVictoire();
				// System.out.println("VALEUR A SUIVRE "+jeux.getValeurAsuivre());
				/*
				 * jeux.victoireJoueur(0); miseTable.setText(""+jeux.getMiseTable());
				 * testAjoutCarte(jeux , action , carteTable , joueurJ , listeJoueur , miseTable
				 * , 1);
				 */
			});
			// action.add(passer, 3, 0);

			// Cr�er un bouton permettant d'afficher l'�tat de partie dans la console
			Button etat = new Button();
			etat.setText("Etat");
			etat.setAlignment(Pos.CENTER);
			etat.setOnAction(e -> {
				jeux.etatPartie();
			});
			// action.add(etat, 3, 1);

			action.add(creerBoutonSeCouche(), 0, 0);
			action.add(creerBoutonSuivre(), 0, 1);
			action.add(creerBoutonMiser(), 1, 0);
			action.add(creerBoutonTapis(), 1, 1);
			action.add(creerBoutonPause(), 2, 1);
			if (!jeux.getCheck())
				action.add(creerBoutonCheck(), 2, 0);
		}
	}

	private void testAjoutCarte(int cptTour) {
		int tour = jeux.getTour();
		int nbCoup = jeux.getNbCoupJouer();
		if (nbCoup >= jeux.getListeJoueur().size() - 1) {
			if (jeux.getTable().size() == 5) {
				affichageListeJoueur(true);
				boolean fin = jeux.testVictoire();
				if (fin) {
					pmvd.setVictory(jeux.getListeJoueur().get(0).getArgent() > 0);
					pmvd.setScore(jeux.getListeJoueur().get(0).getArgent());
					Score.addScore(Score.POKER, jeux.getListeJoueur().get(0).getArgent());
					pmvd.Show();
					action.getChildren().clear();
				} else {
					jeux.setPasserManche(true);
					creerBoutonAction();
				}
								
			} else {
				jeux.setManche(jeux.getManche() + 1);
				jeux.resetStatus();
				jeux.setCheck(false);
				jeux.addCarteTable(1);
				jeux.setValeurAsuivre(20);
				updateMise(jeux);
				ajouteCarteTable(jeux.getTable().size() - 1);
				jeux.setTour(jeux.getManche() % jeux.getListeJoueur().size());
				// retourne faux si tout les joueurs sont couche ou sans argent
				// il faut dans ce cas piocher les 5 cartes
				if (jeux.rotateJoueur()) {
					jeux.setBonTour();
					jeux.setValeurAsuivre(20);
					jeux.setNbCoupJouer(jeux.nombreJoueurCoucher() + jeux.nombreJoueurSansSous());
					affichageListeJoueur(false);
					creerBoutonAction();
				} else {
					testAjoutCarte(cptTour);
				}
			}
		} else {
			jeux.setTour((tour + cptTour) % jeux.getListeJoueur().size());
			jeux.setNbCoupJouer(jeux.getNbCoupJouer() + 1);
			jeux.rotateJoueur();
			jeux.setBonTour();
			affichageListeJoueur(false);
		}
	}

	// Affichage de d�part avec les carte sur la table et les joueurs
	private void affichageDepart() {
		carteTable.getChildren().clear();
		for (int i = 0; i < jeux.getTable().size(); i++)
			ajouteCarteTable(i);

		affichageListeJoueur(false);
	}

	// Ajoute une carte sur la table � partir de l'attribut table et de son
	// num�ro i
	private void ajouteCarteTable(int i) {
		carteTable.getChildren().add(creationImage(HEIGHT_CARD, jeux.getTable().get(i).getName()));
	}

	private void placerJoueur(VBox ligne, int joueur, Poker jeu) {
		int place = joueur - jeu.getTour();
		while (place < 0) {
			place += jeu.getListeJoueur().size();
		}
		grille.getChildren().add(ligne);

		switch (place) {
		case 0:
			GridPane.setConstraints(ligne, 1, 2, 1, 1, HPos.CENTER, VPos.CENTER);
			break;
		case 1:
			GridPane.setConstraints(ligne, 2, 1, 1, 1, HPos.CENTER, VPos.CENTER);
			break;
		case 2:
			GridPane.setConstraints(ligne, 1, 0, 1, 1, HPos.CENTER, VPos.CENTER);
			break;
		case 3:
			GridPane.setConstraints(ligne, 0, 1, 1, 1, HPos.CENTER, VPos.CENTER);
			break;
		default:
			Main.debug("ERR placement:" + place);
			break;
		}
	}

	// Affiche le tableau de joueur sur la droite
	private void affichageListeJoueur(boolean forcerVisible) {

		grille.getChildren().clear();
		grille.getChildren().addAll(table, action);
		//grille.setGridLinesVisible(true);
		GridPane.setConstraints(table, 1, 1, 1, 1, HPos.CENTER, VPos.CENTER);
		GridPane.setConstraints(action, 0, 2, 2, 1);

		int i;
		for (i = 0; i < jeux.getListeJoueur().size(); i++) {
			placerJoueur(creerLigneJoueur(jeux.getListeJoueur().get(i), i == jeux.getTour(), forcerVisible), i, jeux);
		}
		// ajoute des joueur fantome et invisible pour ne pas changer la forme du gridpane
		for (; i < 4; i++) {
			VBox box = creerLigneJoueur(jeux.getListeJoueur().get(0), false, false);
			box.setVisible(false);
			placerJoueur(box, i, jeux);
		}
	}

	/**
	 * Affiche le joueur i dans le tableau a droite en fonction de : si il est
	 * coucher , si il est entrain de jouer
	 * 
	 * @param joueur  : le joueur a afficher
	 * @param fleche  : si on affiche une fleche devant le nom
	 * @param visible : si les cartes doivent etre face visible
	 * @return Une VBox qui represente le joueur
	 */

	private VBox creerLigneJoueur(JoueurPoker joueur, boolean sonTour, boolean forceVisible) {
		String str = joueur.getCouche() ? "(C) " : "      ";
		Label lab = creationLabel(15, str /**/
				+ joueur.getNom() + ": " /**/
				+ joueur.getArgent() + "$");
		lab.setMinWidth(Label.USE_PREF_SIZE);
		lab.setPadding(new Insets(5));
		if (joueur.getStatus()) {
			lab.setTextFill(Color.RED);
		}

		HBox ligne = new HBox(10);
		ligne.setAlignment(Pos.CENTER);
		ligne.setPadding(new Insets(5, 5, 5, 15));
		ligne.setMaxSize(0, 0);

		if (forceVisible) {
			ligne.getChildren().add(creationImage(HEIGHT_CARD, joueur.getMain().get(0).getName()));
			ligne.getChildren().add(creationImage(HEIGHT_CARD, joueur.getMain().get(1).getName()));
		} else {
			ligne.getChildren().add(creationImage(HEIGHT_CARD, ContentManager.IPO_CACHER));
			ligne.getChildren().add(creationImage(HEIGHT_CARD, ContentManager.IPO_CACHER));

			if (sonTour) {
				Timeline tl = new Timeline(new KeyFrame(Duration.seconds(2), e -> {
					ligne.getChildren().clear();
					ligne.getChildren().add(creationImage(HEIGHT_CARD, joueur.getMain().get(0).getName()));
					ligne.getChildren().add(creationImage(HEIGHT_CARD, joueur.getMain().get(1).getName()));
				}));
				tl.setCycleCount(1);
				tl.play();
			}
		}

		VBox box = new VBox(lab, ligne);
		box.setAlignment(Pos.CENTER);
		box.setMaxSize(0, 0);
		box.setBackground(new Background(new BackgroundFill(Color.grayRgb(60), null, null)));
		return box;
	}

	private Label creationLabel(double size, String texte) {
		Label label = new Label(texte);
		label.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, size));
		label.setTextFill(Color.WHITE);
		return label;
	}

	private ImageView creationImage(double height, String name) {
		ImageView image = new ImageView(ContentManager.loadImage(name));
		image.setPreserveRatio(true);
		image.setFitHeight(height);
		return image;
	}
}
