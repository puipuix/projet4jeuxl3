package poker;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Quentin Colras
 */
public class JoueurPoker {
	
	/**
	 * @param nom Nom du joueur
	 * @param main Liste de carte repr�sentant la main du joueur
	 * @param maintable Liste de carte repr�sentant la main du joueur et les cartes sur la table
	 * @param argent L'argent du joueur
	 * @param couche D�finit si le joueur est couch� ou non
	 * @param status D�finit si le joueur check la manche, true si oui 
	 */
	
	public String nom;
	List<Carte> main;
	List<Carte> maintable;
	int argent;
	boolean couche;
	boolean status;
	
	
	/**
	 * 
	 * Constructeur permettant d'instancier un nouveau joueur au numero i
	 */
	public JoueurPoker(int i)
	{
		main = new ArrayList<Carte>();
		maintable = new ArrayList<Carte>();
		argent = 5000;
		nom = ("Joueur "+i);
		status = false;
		couche = false;
	}
	
	/**
	 * Permet d'effacer les deux mains du joueur
	 */
	public void clearMain() {
		main = new ArrayList<Carte>();
		maintable = new ArrayList<Carte>();
	}
	
	/**
	 * Le joueur fait un tapis
	 */
	public int tapis() {
		int somme = argent;
		argent = 0;
		return somme;
	}
	
	/**
	 * Le joueur suit si il peut suivre, si il n'a pas assez d'argent alors il fait un tapis
	 * @param valeur la valeur a suivre
	 */
	public int suivre(int valeur) {
		if(valeur <= argent) {
			argent-=valeur;
			return valeur;
		}
		else return tapis();
	}
	
	/**
	 * Le joueur mise si il n'a pas assez d'argent alors il fait un tapis
	 * @param valeur la valeur a miser
	 */
	public int miser (int valeur) {
		if(valeur<=argent) {
			argent-=valeur;
			return valeur;
		}
		else return tapis();
	}
	

	/**
	 * Permet d'ajouter une carte dans la main d'un joueur
	 * @param c Carte � ajouter
	 */
	public void addCarte(Carte c) {
		main.add(c);
	}
	
	/**
	 * Copie la main envoy� en param�tre dans la main table du joueur
	 * @param main Ensemble de carte � ajouter dans la main du joueur
	 */
	public void copyMain(List<Carte> main) {
		maintable.addAll(main);
	}
	
	public void afficheMain()
	{
		System.out.print("Main du joueur : ");
		for(int i=0;i<main.size();i++)
			System.out.print(main.get(i));
	}
	
	public void afficheMainTable()
	{
		System.out.print("Main du joueur : ");
		for(int i=0;i<maintable.size();i++)
			System.out.print(maintable.get(i));
	}
	
	public void setStatus(boolean b) {
		status = b;
	}
	
	public boolean getStatus() {
		return status;
	}
	
	public List<Carte> getMain(){
		return main;
	}
	
	public List<Carte> getMainTable(){
		return maintable;
	}
	
	public int getArgent() {
		return argent;
	}
	
	public void setArgent(int _argent) {
		argent = _argent;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setCouche(boolean _couche) {
		couche = _couche;
	}
	
	public boolean getCouche() {
		return couche;
	}
	
	@Override
	public String toString() {
		StringBuffer sc = new StringBuffer(getNom());
		sc.append(" ").append(getArgent()).append(" ").append(getStatus());
		return sc.toString();
	}
}
