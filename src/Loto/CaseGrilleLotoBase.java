package Loto;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.TextAlignment;
import projet4jeuxl3.ContentManager;

/**
 * Classe de base pour afficher une case de la grille
 * 
 * @author Alexis
 *
 */
public class CaseGrilleLotoBase extends Group {

	/**
	 * Combinaison de couleurs possibles pour la grille
	 */
	static final Color[][] COLORS = { { Color.DARKSALMON, Color.LIGHTSALMON, Color.DARKSALMON },
			{ Color.BLUE, Color.LIGHTBLUE, Color.BLUE }, { Color.ORANGERED, Color.ORANGE, Color.ORANGERED },
			{ Color.RED, Color.LIGHTSALMON, Color.RED } };

	// taille d'une grille
	static final double SIZE = 50.0;
	// epaisseur de la bordure
	static final double STROKE = 5.0;

	// Index pour les couleurs
	static final int MARKED = 0; // case marque
	static final int UNMARKED = 1; // case non marque
	static final int EMPTY = 2; // case vide

	protected int valeur;
	protected int color;
	protected boolean marked;
	protected Rectangle rectangle;
	protected Label label;

	/**
	 * Construit une case non vide de la grille
	 * 
	 * @param arg0  - texte a ecrire dans la grille
	 * @param color - indice de la couleur a utiliser
	 * @param scale - mise a l'echelle la case
	 */
	public CaseGrilleLotoBase(String arg0, int color, double scale) {
		super();

		valeur = 0;
		this.color = color;
		// background
		rectangle = new Rectangle(SIZE * scale, SIZE * scale, COLORS[color][EMPTY]);
		rectangle.setStroke(COLORS[color][MARKED]);
		rectangle.setStrokeType(StrokeType.INSIDE);
		rectangle.setStrokeWidth(STROKE * scale);
		rectangle.setStrokeLineCap(StrokeLineCap.ROUND);

		// texte
		label = new Label(arg0);
		label.setAlignment(Pos.CENTER);
		label.setTextAlignment(TextAlignment.CENTER);
		label.setLineSpacing(0);
		label.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 17 * scale));
		label.setTranslateX(SIZE / 4 * scale);
		label.setTranslateY(SIZE / 4 * scale);

		// positions
		GridPane.setHalignment(this, HPos.CENTER);
		GridPane.setValignment(this, VPos.CENTER);
		GridPane.setFillHeight(this, true);
		GridPane.setFillWidth(this, true);
		GridPane.setHgrow(this, Priority.ALWAYS);
		GridPane.setVgrow(this, Priority.ALWAYS);
		GridPane.setMargin(this, new Insets(3 * scale));

		this.getChildren().addAll(rectangle, label);
	}

	/**
	 * Construit une case vide
	 * 
	 * @param color - indice de la couleur a utiliser
	 * @param scale - mise a l'echelle la case
	 */
	public CaseGrilleLotoBase(int color, double scale) {
		this("  ", color, scale);
	}

	/**
	 * @return la valeur de la case. 0 pour vide
	 */
	public int getValeur() {
		return valeur;
	}

	/**
	 * @return la couleur de la case.
	 */
	public int getColor() {
		return color;
	}

}
