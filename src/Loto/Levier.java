package Loto;

import java.util.Observable;

import javafx.animation.Animation.Status;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeType;
import javafx.util.Duration;
import projet4jeuxl3.Main;

/**
 * Bouton a l'apparance d'un levier
 * 
 * @author Alexis
 *
 */
public class Levier extends Group {

	private Rectangle back;
	private Rectangle barre;
	private Circle circle;

	private EventHandler<MouseEvent> onStart = e -> {
		System.out.println("ERR levier OnStart");
	};

	private EventHandler<ActionEvent> onFinish = e -> {
		System.out.println("ERR levier OnFinish");
	};

	/**
	 * 
	 * @param duration  - la duree de l'animation
	 * @param size      - la taille du levier
	 * @param focusable - observable pour desactiber le focus
	 * @param onStart   - quand clique
	 * @param onFinish  - quand l'animation est finie
	 */
	public Levier(double duration, double size, Observable focusable, EventHandler<MouseEvent> onStart,
			EventHandler<ActionEvent> onFinish) {
		this(duration, size, focusable);
		this.onStart = onStart;
		this.onFinish = onFinish;
	}

	/**
	 * 
	 * @param duration  - la duree de l'animation
	 * @param size      - la taille du levier
	 * @param focusable - observable pour desactiber le focus
	 */
	public Levier(double duration, double size, Observable focusable) {
		super();
		this.back = new Rectangle(size, 3 * size, Color.DIMGREY);
		back.setStroke(Color.INDIANRED);
		back.setTranslateX(-size / 2);
		back.setTranslateY(-size * 3 / 2);
		back.setStrokeType(StrokeType.INSIDE);
		back.setStrokeWidth(size / 3);
		back.setStrokeLineCap(StrokeLineCap.ROUND);

		this.barre = new Rectangle(size / 3.5, size, Color.GRAY);
		barre.setStroke(Color.LIGHTGREY);
		barre.setTranslateX(-size / 7);
		barre.setTranslateY(-size);
		barre.setStrokeType(StrokeType.INSIDE);
		barre.setStrokeWidth(size / 3 / 4);
		barre.setStrokeLineCap(StrokeLineCap.BUTT);

		this.circle = new Circle(size / 2.1, Color.RED);
		circle.setTranslateY(-size);
		circle.setStroke(Color.BLACK);
		circle.setStrokeType(StrokeType.INSIDE);
		circle.setStrokeWidth(size / 21);

		KeyFrame downCircle = new KeyFrame(Duration.seconds(duration / 2),
				new KeyValue(circle.translateYProperty(), size));
		KeyFrame upCircle = new KeyFrame(Duration.seconds(duration), new KeyValue(circle.translateYProperty(), -size));

		KeyFrame downBarreStart = new KeyFrame(Duration.seconds(duration / 4), new KeyValue(barre.heightProperty(), 0),
				new KeyValue(barre.translateYProperty(), 0));
		KeyFrame downBarreEnd = new KeyFrame(Duration.seconds(duration / 2), new KeyValue(barre.heightProperty(), size),
				new KeyValue(barre.translateYProperty(), 0));

		KeyFrame upBarreStart = new KeyFrame(Duration.seconds(3 * duration / 4),
				new KeyValue(barre.heightProperty(), 0), new KeyValue(barre.translateYProperty(), 0));
		KeyFrame upBarreEnd = new KeyFrame(Duration.seconds(duration), new KeyValue(barre.heightProperty(), size),
				new KeyValue(barre.translateYProperty(), -size));

		KeyFrame end = new KeyFrame(Duration.seconds(duration), e -> {
			onFinish.handle(e);
		});

		Timeline line = new Timeline(downCircle, upCircle, downBarreStart, downBarreEnd, upBarreStart, upBarreEnd, end);
		circle.setOnMouseClicked(e -> {
			if (circle.isFocusTraversable()) {
				circle.requestFocus();
				if (!(line.getStatus() == Status.RUNNING)) {
					onStart.handle(e);
					line.play();
				}
			}
		});

		circle.setOnKeyReleased(e -> {
			if (e.getCode() == KeyCode.ENTER && circle.isFocusTraversable())
				circle.getOnMouseClicked().handle(null);
		});

		focusable.addObserver((v, traversable) -> {
			circle.setFocusTraversable((boolean) traversable);
		});
		circle.setFocusTraversable(true);

		Main.ApplyFocusEffect(circle);
		this.getChildren().addAll(back, barre, circle);
	}

	/**
	 * @return Quand le levier est clique
	 */
	public EventHandler<MouseEvent> getOnStart() {
		return onStart;
	}

	/**
	 * @param onStart - Quand le levier est clique
	 */
	public void setOnStart(EventHandler<MouseEvent> onStart) {
		this.onStart = onStart;
	}

	/**
	 * @return Quand l'animation est finie
	 */
	public EventHandler<ActionEvent> getOnFinish() {
		return onFinish;
	}

	/**
	 * @param onFinish - Quand l'animation est finie
	 */
	public void setOnFinish(EventHandler<ActionEvent> onFinish) {
		this.onFinish = onFinish;
	}
}
