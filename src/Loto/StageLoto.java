package Loto;

import java.util.Observable;
import java.util.Random;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
import projet4jeuxl3.ContentManager;
import projet4jeuxl3.MultipleRoot;
import projet4jeuxl3.PauseMenuBase;
import projet4jeuxl3.PauseMenuJeux;
import projet4jeuxl3.PauseMenuVictoireDefaite;
import projet4jeuxl3.Score;

/**
 * Stage pour le jeu du loto
 * 
 * @author Alexis
 *
 */
public class StageLoto extends Stage {

	static final boolean TRICHE = false;

	static final double TIRAGE_DURATION = 1.0;
	static final double SCALE_GRILLE_IA = 0.5;

	static final double IA_MARKTIME_MIN = .5;
	static final double IA_MARKADD = 2.0;
	static final double IA_CHECKTIME = 1.0;

	private Loto loto;
	private GrilleLoto[] grilles;

	private ObservableFocus focusable = new ObservableFocus();

	/**
	 * Permet de desactiver le focus si un menu apparait
	 * 
	 * @author Alexis
	 *
	 */
	public class ObservableFocus extends Observable {
		/**
		 * Notifie les observer du changement
		 * 
		 * @param v - si on peut active le focus
		 */
		public void notifyObservers(boolean v) {
			setChanged();
			super.notifyObservers(v);
		}
	}

	private Button carton;
	private Button pause;

	private boolean isFinish = false;

	/**
	 * Construit un loto avec 3 ia
	 */
	public StageLoto() {
		this(3);
	}

	/**
	 * Construit un loto
	 * 
	 * @param nbIa - le nombre d'ia
	 */
	public StageLoto(int nbIa) {

		MultipleRoot root = new MultipleRoot();

		this.setTitle("Loto !");

		// ecran de chargement
		Label titre = new Label("Loto !");
		titre.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 50));
		titre.setTextFill(Color.WHITE);
		titre.setPadding(new Insets(10));

		Label sub = new Label("Chargement...");
		sub.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 20));
		sub.setTextFill(Color.YELLOW);
		sub.setPadding(new Insets(10));

		VBox pane = new VBox(titre, sub);
		pane.setAlignment(Pos.CENTER);
		root.setMainRoot(pane);
		Scene sc = new Scene(root, Color.rgb(50, 50, 50));
		this.setScene(sc);
		this.show();
		// permet d'afficher l'ecran de chargement puis de commencer a charger le jeu
		Platform.runLater(() -> {
			create(root, nbIa);
			// redimention automatique de la fenetre
			hide();
			show();
		});
	}

	// construit l'affichage
	private void create(MultipleRoot root, int nbIa) {
		// init grille / loto
		this.loto = new Loto();
		grilles = new GrilleLoto[nbIa + 1];

		HBox pane = new HBox();
		pane.setAlignment(Pos.CENTER);
		pane.setBackground(Background.EMPTY);

		grilles[0] = new GrilleLoto(1.0, true, focusable);
		for (int i = 1; i <= nbIa; i++) {
			grilles[i] = new GrilleLoto(SCALE_GRILLE_IA, false, focusable);
		}

		PauseMenuVictoireDefaite menuFin = new PauseMenuVictoireDefaite(this, () -> {
			return new StageLoto();
		});

		// IAs
		VBox left = new VBox(5);
		left.setAlignment(Pos.CENTER);
		left.setBackground(new Background(new BackgroundFill(Color.GREY, null, new Insets(10))));
		BorderPane.setAlignment(left, Pos.CENTER);

		for (int i = 1; i <= nbIa; i++) {
			VBox player = new VBox(5);
			player.setPadding(new Insets(15));
			player.setAlignment(Pos.CENTER_LEFT);
			Label lab = new Label("Joueur " + (i + 1));
			lab.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 20));
			player.getChildren().addAll(lab, grilles[i]);

			left.getChildren().add(player);
		}

		// Joueur
		VBox right = new VBox(10);
		right.setAlignment(Pos.CENTER);
		right.setPadding(new Insets(0, 10, 0, 0));

		HBox menu = new HBox(5);
		menu.setAlignment(Pos.CENTER);

		// menu ingame
		pause = new Button("Pause");
		pause.setMinWidth(Button.USE_PREF_SIZE);
		PauseMenuJeux menuPause = new PauseMenuJeux(this);
		EventHandler<ActionEvent> ae = menuPause.getRetour().getOnAction();

		pause.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 25));
		pause.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER)
				pause.fire();
		});

		carton = new Button("CARTON PLEIN !");
		carton.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 25));
		carton.setTextAlignment(TextAlignment.CENTER);
		carton.setMinWidth(Button.USE_PREF_SIZE);

		menuPause.getRetour().setOnAction(e -> {
			ae.handle(e);
			setFocus(true);
		});
		pause.setOnAction(e -> {
			menuPause.Show();
			setFocus(false);
		});

		carton.setOnAction(e -> {
			if (!isFinish) {
				if (grilles[0].cartonPlein(loto.getValides())) {
					menuFin.setVictory(true);
					menuFin.setSubTitle("Vous avez gagn� !");
					menuFin.setScore(10000);
					Score.addScore(Score.LOTO, 10000);
				} else {
					menuFin.setVictory(false);
					menuFin.setSubTitle("Votre grille n'est pas valide.");
					menuFin.setScore(0);
					Score.addScore(Score.LOTO, 0);
				}
				isFinish = true;
				setFocus(false);
				menuFin.Show();
			}
		});
		carton.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER)
				carton.fire();
		});

		// triche
		Button ttTirer = new Button("TT");
		ttTirer.setOnAction(e -> {
			for (int i = 0; i < loto.getValides().length; i++)
				loto.getValides()[i] = true;
		});

		// triche
		Button tirerVictoire = new Button("TPV");
		tirerVictoire.setOnAction(e -> {
			for (int i = 0; i < grilles[0].getValeurs().length; i++) {
				for (int y = 0; y < grilles[0].getValeurs()[i].length; y++) {
					int v = grilles[0].getValeurs()[i][y].getValeur();
					if (v != 0) {
						loto.getValides()[v] = true;
					}
				}
			}
		});

		VBox buttons = new VBox(5, pause, carton);
		// boutons triche:
		if (TRICHE) {
			buttons.getChildren().addAll(ttTirer, tirerVictoire);
		}

		buttons.setAlignment(Pos.CENTER);
		BorderPane.setAlignment(buttons, Pos.CENTER);

		// levier et display

		LotoResultDisplay display = new LotoResultDisplay(300);

		Levier levier = new Levier(TIRAGE_DURATION, 100, focusable, e -> {
			if (loto.isEmpty()) {
				setFocus(false);
				menuFin.setVictory(false);
				menuFin.setScore(0);
				Score.addScore(Score.LOTO, 0);
				menuFin.setSubTitle("Il n'y plus de boule � tirer.");
				menuFin.Show();
			} else {
				display.randomEffect(TIRAGE_DURATION);
			}
		}, e -> {
			int v = loto.tirer();
			display.setNumber(v);
			checkVictories(nbIa, menuFin, v);
		});

		menu.getChildren().addAll(buttons, display, levier);

		right.getChildren().addAll(menu, grilles[0]);

		pane.getChildren().addAll(left, right);

		root.getSubRoots().clear();
		root.setMainRoot(pane);
		root.getSubRoots().addAll(menuFin, menuPause);
		// Redimentionne le gridPane si on change la taille de la fenetre
		// multRoot.resizeMainRoot(true); n'est pas appel� avant car tout les �l�ments
		// n'ont pas encore pris leurs taille ce qui pose des probl�mes
		ChangeListener<Number> resize = new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (observable.equals(heightProperty())) {
					setMinHeight(getHeight());
					heightProperty().removeListener(this);
				} else {
					setMinWidth(getWidth());
					widthProperty().removeListener(this);
				}
				root.resizeMainRoot(true);
			}
		};
		this.heightProperty().addListener(resize);
		this.widthProperty().addListener(resize);
	}

	private void checkVictories(int nbIa, PauseMenuVictoireDefaite menuFin, int lastValue) {
		// nombre de gagant
		Random r = new Random();
		for (int i = 1; i <= nbIa; i++) {
			final GrilleLoto current = grilles[i];
			final int player = i;
			double markTime = IA_MARKTIME_MIN + r.nextDouble() * IA_MARKADD;
			Timeline tl = new Timeline(new KeyFrame(Duration.seconds(markTime), e -> {
				current.markIfExist(lastValue);
			}), new KeyFrame(Duration.seconds(markTime + r.nextDouble() * IA_CHECKTIME), e -> {
				if (current.cartonPlein(loto.getValides())) {
					if (!isFinish) {
						isFinish = true;
						Runnable[] toRun = new Runnable[1];
						Runnable run = () -> {
							setFocus(false);
							menuFin.setVictory(false);
							menuFin.setSubTitle("Le joueur " + player + " � gagn�");
							menuFin.setScore(0);
							if (PauseMenuBase.isMenuVisible()) {
								Platform.runLater(toRun[0]);
							} else {
								menuFin.Show();
								Score.addScore(Score.LOTO, 0);
							}
						};
						toRun[0] = run;
						run.run();
					}
				}
			}));
			tl.setCycleCount(1);
			tl.play();
		}
	}

	private void setFocus(boolean b) {
		focusable.notifyObservers(b);
		pause.setFocusTraversable(b);
		carton.setFocusTraversable(b);
	}
}
