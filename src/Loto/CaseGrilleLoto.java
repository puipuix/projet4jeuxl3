package Loto;

import java.util.Observable;

import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import projet4jeuxl3.Main;

/**
 * Case d'une grille avec une valeur et qui peut �tre marqu� par le joueur.
 * 
 * @author Alexis
 *
 */
public class CaseGrilleLoto extends CaseGrilleLotoBase {

	/**
	 * Construit une case
	 * 
	 * @param valeur    - valeur de la case
	 * @param color     - indice de la couleur
	 * @param scale     - mise a l'echelle
	 * @param clickable - si l'utilisateur peut cliquer sur la case
	 * @param focusable - permet de savoir si un ecran de pause est affiche
	 */
	public CaseGrilleLoto(int valeur, int color, double scale, boolean clickable, Observable focusable) {
		super(Integer.toString(valeur), color, scale);
		super.valeur = valeur;

		// recentre
		label.setTranslateX((SIZE / 4 + (valeur > 9 ? -4 : 4)) * scale);
		UnMark();

		setOnMouseClicked(e -> {
			// si on peut cliquer et qu'il n'y a pas d'ecran
			if (clickable && isFocusTraversable()) {
				switchMark();
			}
		});

		setOnKeyReleased(e -> {
			// si on peut cliquer et qu'il n'y a pas d'ecran
			if (clickable && isFocusTraversable() && e.getCode() == KeyCode.ENTER) {
				switchMark();
			}
		});

		focusable.addObserver((v, traversable) -> {
			// si on peut cliquer et qu'il n'y a pas d'ecran
			setFocusTraversable((boolean) traversable && clickable);
		});
		setFocusTraversable(clickable);

		Main.ApplyFocusEffect(this);
	}

	/**
	 * inverse le marquage
	 */
	public void switchMark() {
		if (marked) {
			UnMark();
		} else {
			Mark();
		}
	}

	/**
	 * @return si la case est marque
	 */
	public boolean IsMarked() {
		return marked;
	}

	/**
	 * Marque la case
	 */
	public void Mark() {
		marked = true;
		rectangle.setFill(COLORS[color][MARKED]);
		rectangle.setStroke(COLORS[color][UNMARKED]);
		label.setTextFill(Color.WHITE);
	}

	/**
	 * demarque la case
	 */
	public void UnMark() {
		marked = false;
		rectangle.setFill(COLORS[color][UNMARKED]);
		rectangle.setStroke(COLORS[color][MARKED]);
		label.setTextFill(Color.BLACK);
	}
}
