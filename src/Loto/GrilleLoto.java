package Loto;

import java.util.LinkedList;
import java.util.Observable;
import java.util.Random;

import javafx.geometry.Insets;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

/**
 * 
 * @author Alexis
 *
 */
public class GrilleLoto extends GridPane {

	private CaseGrilleLotoBase[][] valeurs;

	/**
	 * cree une grille de loto
	 * 
	 * @param scale     - mise a l'echelle
	 * @param clickable - si l'utilisateur peut cliquer sur la case
	 * @param focusable - permet de savoir si un ecran de pause est affiche
	 */
	public GrilleLoto(double scale, boolean clickable, Observable focusable) {

		setGridLinesVisible(true);
		setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, new BorderWidths(5), new Insets(5))));
		setMaxHeight(USE_PREF_SIZE);
		setMaxWidth(USE_PREF_SIZE);
		setMinHeight(USE_PREF_SIZE);
		setMinWidth(USE_PREF_SIZE);

		// on recupere la liste des pions melange
		LinkedList<Integer> p = Loto.getRandomPion();

		valeurs = new CaseGrilleLotoBase[9][3];
		// couleur aleatoire
		int couleur = new Random().nextInt(CaseGrilleLotoBase.COLORS.length);

		// On rempli chaque ligne avec des valeurs...
		for (int y = 0; y < 3; y++) {
			int place = 0; // nombre de valeur place
			// tant qu'on a pas place 5 valeurs
			while (place < 5) {
				// on tire une valeur
				int v = p.removeFirst();

				// si la case est libre
				if (aviable(v, y)) {
					// alors cree une case avec une valeur
					valeurs[indexOfValue(v)][y] = new CaseGrilleLoto(v, couleur, scale, clickable, focusable);
					place++;
				} else {
					// sinon on remet la valeur a la fin pour les autres lignes
					p.addLast(v);
				}
			}
		}

		// pour chaque case
		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 9; x++) {
				// si elle n'a pas ete initialise
				if (valeurs[x][y] == null) {
					// alors on cree une case vide
					valeurs[x][y] = new CaseGrilleLotoBase(couleur, scale);
				}
				// on ajoute la case au gridpane
				this.add(valeurs[x][y], x, y);
			}
		}
	}

	/**
	 * @param v - la valeur de la case
	 * @return l'indice de la colonne ou se situe la valeur
	 */
	public static int indexOfValue(int v) {
		return (v - 1) / 10;
	}

	/**
	 * @param v - la valeur
	 * @param l - la ligne
	 * @return si la case est vide
	 */
	private boolean aviable(int v, int l) {
		if (valeurs[indexOfValue(v)][l] == null) {
			return true;
		} else {
			return valeurs[indexOfValue(v)][l].getValeur() == 0;
		}
	}

	/**
	 * @param pionValides - la liste des pions
	 * @param l           - l'indice de la ligne
	 * @return si la ligne est complete
	 */
	public boolean isLigneRemplie(boolean[] pionValides, int l) {
		boolean win = true;
		for (int x = 0; x < 9 && win; x++) {
			win = pionValides[Math.abs(valeurs[x][l].getValeur())];
		}
		return win;
	}

	/**
	 * @param pionValides - la liste des pions
	 * @return le nombre de lignes completes
	 */
	public int nbLigneRemplies(boolean[] pionValides) {
		int nb = 0;
		for (int y = 0; y < 3; y++) {
			nb += isLigneRemplie(pionValides, y) ? 1 : 0;
		}
		return nb;
	}

	/**
	 * @param pionValides - la liste des pions
	 * @return si une ligne au moins est complete
	 */
	public boolean quinte(boolean[] pionValides) {
		return nbLigneRemplies(pionValides) >= 1;
	}

	/**
	 * @param pionValides - la liste des pions
	 * @return si au moins 2 ligne sont completes
	 */
	public boolean doubleQuinte(boolean[] pionValides) {
		return nbLigneRemplies(pionValides) >= 2;
	}

	/**
	 * @param pionValides - la liste des pions
	 * @return si le carton est complet
	 */
	public boolean cartonPlein(boolean[] pionValides) {
		return nbLigneRemplies(pionValides) >= 3;
	}

	/**
	 * Marque la case si la valeur est dans la grille
	 * 
	 * @param value - la valeur a teste
	 */
	public void markIfExist(int value) {
		boolean find = false;
		for (int l = 0; l < 3 && !find; l++) {
			CaseGrilleLotoBase base = valeurs[indexOfValue(value)][l];
			if (base.getValeur() == value && base instanceof CaseGrilleLoto) {
				CaseGrilleLoto caseloto = (CaseGrilleLoto) base;
				caseloto.Mark();
			}
		}
	}

	@Override
	public String toString() {
		StringBuffer b = new StringBuffer();
		for (int y = 0; y < 3; y++) {
			b.append('|');
			for (int x = 0; x < 9; x++) {
				b.append(valeurs[x][y].getValeur() == 0 ? "_" : valeurs[x][y].getValeur());
				b.append(' ');
			}
			b.append("|\n");
		}
		return b.toString();
	}

	/**
	 * @return la grille
	 */
	public CaseGrilleLotoBase[][] getValeurs() {
		return valeurs;
	}
}
