package Loto;

import java.util.Random;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import projet4jeuxl3.ContentManager;

/**
 * Affiche le resultat du tirage
 *
 */
public class LotoResultDisplay extends HBox {

	static final Duration DURATION = Duration.seconds(0.1);

	Label left, right;
	Timeline randomDisplay;
	Random random;

	/**
	 * 
	 * @param size - la taille de l'affichage
	 */
	public LotoResultDisplay(double size) {
		super(-3);
		setAlignment(Pos.CENTER);

		// label de gauche
		left = new Label("");
		left.setAlignment(Pos.CENTER);
		left.setTextAlignment(TextAlignment.CENTER);
		left.setFont(ContentManager.LoadFont(ContentManager.FONT_NUMERIC, size));
		left.setTextFill(Color.YELLOW);
		setSize(left, size);
		left.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, new BorderWidths(6), new Insets(0))));

		// label de droite
		right = new Label("");
		right.setAlignment(Pos.CENTER);
		right.setTextAlignment(TextAlignment.CENTER);
		right.setFont(ContentManager.LoadFont(ContentManager.FONT_NUMERIC, size));
		right.setTextFill(Color.YELLOW);
		setSize(right, size);
		right.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, new BorderWidths(6), new Insets(0))));

		getChildren().addAll(left, right);

		random = new Random();
		// animation de tirage
		randomDisplay = new Timeline(new KeyFrame(Duration.seconds(0), e -> {
			setNumber(1 + random.nextInt(90));
		}), new KeyFrame(DURATION, e -> {
			setNumber(0);
		}));
	}

	// force la taille du label
	private void setSize(Label but, double size) {
		but.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, null, null)));
		but.setPrefHeight(size);
		but.setPrefWidth(size * 0.55);
		but.setMaxHeight(size);
		but.setMaxWidth(size * 0.55);
		but.setMinHeight(size);
		but.setMinWidth(size * 0.55);
	}

	/**
	 * lance l'animation de tirage pour la duree donnee
	 * 
	 * @param sec - la duree
	 */
	public void randomEffect(double sec) {
		randomDisplay.setCycleCount((int) (sec / DURATION.toSeconds()));
		randomDisplay.play();
	}

	/**
	 * ecrit un nombre dans l'affichage
	 * 
	 * @param n - le nombre. 0 pour ne rien afficher
	 */
	public void setNumber(int n) {
		// si on affiche rien
		if (n == 0) {
			left.setText("");
			right.setText("");
		} else {
			// sinon on decoupe le nombre pour que la dixaine soit a gauche et l'unite a
			// droite
			int v = n / 10;
			left.setText(v != 0 ? Integer.toString(v) : "");
			right.setText(Integer.toString(n % 10));
		}
	}
}
