package Loto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Gestion du tirage
 * 
 * @author Alexis
 *
 */
public class Loto {

	// liste des pions
	private boolean[] valides = new boolean[91];
	// liste des pions non tires
	private LinkedList<Integer> reserve;

	/**
	 * 
	 */
	public Loto() {
		super();
		valides[0] = true;
		reserve = getRandomPion();
	}

	/**
	 * @return une liste des pions possible melange
	 */
	public static LinkedList<Integer> getRandomPion() {
		ArrayList<Integer> a = new ArrayList<>();
		for (int i = 1; i <= 90; i++) {
			a.add(i);
		}
		Collections.shuffle(a);
		Collections.shuffle(a);
		Collections.shuffle(a);
		Collections.shuffle(a);
		Collections.shuffle(a);

		return new LinkedList<Integer>(a);
	}

	/**
	 * @return si il n'y a plus de valeur a tirer
	 */
	public boolean isEmpty() {
		return reserve.isEmpty();
	}

	/**
	 * @return la liste des pions
	 */
	public boolean[] getValides() {
		return valides;
	}

	/**
	 * tire une valeur
	 * 
	 * @return la valeur tire
	 */
	public int tirer() {
		if (reserve.isEmpty()) {
			return 0;
		} else {
			int a = reserve.removeFirst();
			valides[a] = true;
			return a;
		}
	}
}
