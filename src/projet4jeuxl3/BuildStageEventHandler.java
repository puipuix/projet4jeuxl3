package projet4jeuxl3;

import javafx.stage.Stage;

/**
 * 
 * @author Alexis
 *
 */
public interface BuildStageEventHandler {

	/**
	 * cree nouveau stage.
	 * @return le stage cree.
	 */
	public Stage Build();
}
