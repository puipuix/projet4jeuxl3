package projet4jeuxl3;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * 
 * @author Alexis
 *
 */
public class StageScore extends Stage {

	/**
	 * Taille du texte 
	 */
	public static final double SCORE_TITLE_SIZE = 15;
	/**
	 * Taille des chiffres
	 */
	public static final double SCORE_SCORE_SIZE = 17;
	
	/**
	 * Distance d'apparition des labels
	 */
	public static final double SPAWN_DISTANCE = 2048;
	/**
	 * Temps d'attente
	 */
	public static final double START_TL = 0.15;
	/**
	 * Temps entre chaque arrive des labels
	 */
	public static final double INC_TL = 0.30;
	
	/**
	 * Couleur du fond
	 */
	public static final Color BACKGROUND = Color.rgb(100, 100, 100);

	/**
	 * 
	 */
	public StageScore() {

		// retour accueil
		BoutonFenetreSuivante button = new BoutonFenetreSuivante("Retour", this, () -> {
			return new Accueil();
		});
		button.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 18));
		button.setMaxWidth(Double.MAX_VALUE);
		button.setBorder(Border.EMPTY);

		// scores
		Accordion acc = new Accordion();
		HBox total = buildHB("Score global:", Score.getTotalScore());
		total.setTranslateX(0);
		buildTile("Loto:", Score.LOTO, acc, button);
		buildTile("Poker:", Score.POKER, acc, button);
		buildTile("Sudoku:", Score.SUDOKU, acc, button);
		buildTile("Bataille navale:", Score.BN, acc, button);

		VBox pane = new VBox(total, acc, button);
		pane.setBackground(new Background(new BackgroundFill(BACKGROUND, null, null)));
		setScene(new Scene(pane));
		int step[] = new int[] { 0 };
		double diff[] = new double[1];
		// redimentionne la fenetre
		acc.heightProperty().addListener((v, o, n) -> {
			if (step[0] == 1) {
				// on sauvegarde la difference de taille entre la fenetre et les elements
				diff[0] = getHeight() - o.doubleValue() - button.getHeight() - total.getHeight();
				step[0] = 2;
			}
			if (step[0] == 0) {
				// on ne fait rien -> tout n'est pas forcement cree
				step[0] = 1;
			} else {
				// on change la taille de la fenetre en prenant en compte la difference calcule plus tot
				setHeight(n.doubleValue() + button.getHeight() + total.getHeight() + diff[0]);
			}
		});
		ChangeListener<Number> resize = new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (observable.equals(heightProperty())) {
					setMinHeight(getHeight());
					heightProperty().removeListener(this);
				} else {
					setMinWidth(getWidth());
					widthProperty().removeListener(this);
				}
			}
		};
		this.heightProperty().addListener(resize);
		this.widthProperty().addListener(resize);
	}

	/**
	 * Construit une box avec un titre et un score
	 */
	private HBox buildHB(String title, long score) {
		Label t = new Label(title);
		t.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, SCORE_TITLE_SIZE));
		t.setTextFill(Color.LIGHTGREY);
		Label s = new Label("" + score);
		s.setFont(ContentManager.LoadFont(ContentManager.FONT_NUMERIC, SCORE_SCORE_SIZE));
		s.setTextFill(Color.LIGHTGREY);
		HBox box = new HBox(t, s);
		box.setAlignment(Pos.CENTER);
		box.setSpacing(5);
		box.setTranslateX(SPAWN_DISTANCE);
		box.setBackground(new Background(new BackgroundFill(Color.rgb(70, 70, 70), null, null)));
		box.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, BorderStroke.MEDIUM)));

		return box;
	}

	/**
	 * construit la TitledPane pour un jeu
	 */
	private void buildTile(String gameName, int game, Accordion acc, Button focusNext) {
		Timeline tl = new Timeline();
		double end = START_TL;
		HBox ttl = buildHB("Total :", Score.getData(game, Score.TOTAL));
		tl.getKeyFrames().add(new KeyFrame(Duration.seconds(end += INC_TL), new KeyValue(ttl.translateXProperty(), 0)));

		HBox nb = buildHB("Parties :", Score.getData(game, Score.NB));
		tl.getKeyFrames().add(new KeyFrame(Duration.seconds(end += INC_TL), new KeyValue(nb.translateXProperty(), 0)));

		HBox avg = buildHB("Moyenne :", Score.getData(game, Score.AVG));
		tl.getKeyFrames().add(new KeyFrame(Duration.seconds(end += INC_TL), new KeyValue(avg.translateXProperty(), 0)));

		HBox last = buildHB("Dernier :", Score.getData(game, Score.LAST));
		tl.getKeyFrames()
				.add(new KeyFrame(Duration.seconds(end += INC_TL), new KeyValue(last.translateXProperty(), 0)));

		VBox vb = new VBox(ttl, nb, avg, last);
		vb.setBackground(new Background(new BackgroundFill(BACKGROUND, null, null)));
		TitledPane pane = new TitledPane(gameName, vb);
		pane.setTextFill(Color.grayRgb(50));
		pane.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, SCORE_TITLE_SIZE));
		
		// apparition des fenetres
		final ChangeListener<Boolean> expand = new ChangeListener<Boolean>() {
			boolean first = true;

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (first) {
					// skip lors du chargement
					first = false;
				} else {
					tl.setCycleCount(1);
					tl.play();
					pane.expandedProperty().removeListener(this);
				}
			}
		};
		pane.expandedProperty().addListener(expand);
		
		
		pane.setOnKeyPressed(e -> {
			// Etend la tile si la touche entree est presse
			if (e.getCode() == KeyCode.ENTER) {
				pane.setExpanded(!pane.isExpanded());
				// si c'est la derniere tile on focus le bouton retour
			} else if (e.getCode() == KeyCode.DOWN && acc.getPanes().indexOf(pane) == (acc.getPanes().size() - 1)) {
				focusNext.requestFocus();
			}
		});
		acc.getPanes().add(pane);
	}
}
