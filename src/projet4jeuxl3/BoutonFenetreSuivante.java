package projet4jeuxl3;

import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

/**
 * 
 * @author Alexis
 *
 */
public class BoutonFenetreSuivante extends Button {

	/**
	 * Le stage a fermer.
	 */
	Stage old;

	/**
	 * Sert a creer un nouveau stage.
	 */
	BuildStageEventHandler next;

	/**
	 * Cree un bouton qui change de fenetre quand il est clique.
	 * 
	 * @param txt  - le texte du bouton
	 * @param old  - le stage a fermer
	 * @param next - un EventHandler pour creer un nouvea stage et le retourner
	 */
	public BoutonFenetreSuivante(String txt, Stage old, BuildStageEventHandler next) {
		super(txt);
		this.old = old;
		this.next = next;

		setOnAction(e -> {
			old.hide(); // cache l'ancien
			next.Build().show(); // cree et affiche ne nouveau stage
		});

		// autorise la touche enter pour activer le bouton
		setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER)
				fire();
		});
	}
}
