package projet4jeuxl3;

import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Applique un ecran noir par dessus les jeux, permet d'avoir un menu
 * 
 * @author Alexis
 *
 */
public class PauseMenuBase extends BorderPane {

	/** si un menu est visible */
	private static boolean menuVisible;

	protected Rectangle back;
	protected VBox menu;

	/**
	 * Construit l'affichage et place le menu d'entree par dessus l'ecran noir
	 * 
	 * @param menu
	 */
	public PauseMenuBase(VBox menu) {
		setVisible(false);
		
		// ecran noir
		back = new Rectangle();
		back.setFill(Color.BLACK);
		back.setOpacity(0.7);
		this.menu = menu;

		getChildren().add(back);
		setCenter(menu);
		menu.setAlignment(Pos.CENTER);
		menu.setSpacing(10);

		this.heightProperty().addListener((v, o, n) -> {
			back.setHeight(n.doubleValue());
		});
		this.widthProperty().addListener((v, o, n) -> {
			back.setWidth(n.doubleValue());
		});
	}

	/**
	 * Montre le menu
	 */
	public void Show() {
		if (!menuVisible) { // s'il n'y a pas d'ecran visible
			setVisible(true);
			menuVisible = true;
		}
	}

	/**
	 * Cache le menu
	 */
	public void Hide() {
		if (isVisible()) {
			setVisible(false);
			menuVisible = false;
		}
	}

	/**
	 * @return si un menu est visible
	 */
	public static boolean isMenuVisible() {
		return menuVisible;
	}

	protected static void setMenuVisible(boolean menuVisible) {
		PauseMenuBase.menuVisible = menuVisible;
	}

}
