package projet4jeuxl3;

import java.io.File;
import java.io.Serializable;

/**
 * 
 * @author Alexis
 *
 */
public class Score implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3048608401709613293L;

	/**
	 * INDEX LOTO
	 */
	public static int LOTO = 0;

	/**
	 * INDEX POKER
	 */
	public static int POKER = 1;
	/**
	 * INDEX SUDOKU
	 */
	public static int SUDOKU = 2;
	/**
	 * INDEX BATAILLE NAVALE
	 */
	public static int BN = 3;
	/**
	 * INDEX SCORE TOTAL
	 */
	public static int TOTAL = 0;
	/**
	 * INDEX SCORE DERNIERE BATAILLE
	 */
	public static int LAST = 1;
	/**
	 * INDEX MOYENNE
	 */
	public static int AVG = 2;
	/**
	 * INDEX NOMBRE DE PARTIES
	 */
	public static int NB = 3;

	private long scores[][] = new long[16][16];

	/**
	 * Le singleton du score
	 */
	public static Score S;

	static {
		// chargement de la sauvegarde
		File f = new File(ContentManager.SCORE_FILE);
		if (f.exists()) {
			try {
				S = ContentManager.loadObject(f);
			} catch (Exception e) {
				e.printStackTrace();
				S = new Score();
			}
		} else {
			S = new Score();
		}
	}

	/**
	 * Sauvegarde les scores
	 */
	public static void save() {
		try {
			ContentManager.saveObejct(new File(ContentManager.SCORE_FILE), S);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void setData(int game, int data, long value) {
		S.scores[game][data] = value;
	}

	/**
	 * Retourne un score
	 * 
	 * @param game - Index du jeu
	 * @param data - Index du score
	 * @return Le score
	 */
	public static long getData(int game, int data) {
		return S.scores[game][data];
	}

	/**
	 * @return le score total
	 */
	public static long getTotalScore() {
		return getData(BN, TOTAL) + getData(LOTO, TOTAL) + getData(SUDOKU, TOTAL) + getData(POKER, TOTAL);
	}

	/**
	 * Ajoute le resultat d'une partie
	 * 
	 * @param game  - L'index du jeu
	 * @param score - La valeur du score
	 */
	public static void addScore(int game, long score) {
		setData(game, LAST, score);
		setData(game, TOTAL, score + getData(game, TOTAL));
		setData(game, NB, 1 + getData(game, NB));
		setData(game, AVG, getData(game, TOTAL) / getData(game, NB));
		save();
	}
}
