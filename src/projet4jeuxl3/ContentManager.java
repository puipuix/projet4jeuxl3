package projet4jeuxl3;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.text.Font;

/**
 * Gestionaire des fichiers
 * 
 * @author Alexis
 *
 */
public class ContentManager {

	public static final String SCORE_FILE = "Score.data";

	// pas de constructeur
	private ContentManager() {
	}

	/**
	 * Cherche dans chaque fichier source et retourne un imputStream du fichier name
	 * 
	 * @param name - le fichier a charger
	 * @return L'inputStream du fichier
	 */
	public static InputStream getInputStream(String name) {
		return ContentManager.class.getResourceAsStream(name.startsWith("/") ? name : "/" + name);
	}

	public static String getRessource(String name) {
		return ContentManager.class.getResource(name).toExternalForm();
	}

	/**
	 * Cree un nouveau objet a partir d'un nom de fichier
	 * 
	 * @param <T>    - le type de l'objet a creer, doit avoir un constructeur avec
	 *               un inputStream en parametre (seul).
	 * @param name   - le nom du fichier a charger
	 * @param classe - la classe du type a charger
	 * @return un nouvel objet
	 */
	public static <T> T load(String name, Class<T> classe) {
		try {
			return classe.getConstructor(InputStream.class).newInstance(getInputStream(name));
		} catch (Exception e) {
			InstantiationException i = new InstantiationException("The object cannot be loaded: " + name);
			i.initCause(e);
			i.printStackTrace();
			return null;
		}
	}

	/**
	 * Sauvegarde un object dans un fichier
	 * 
	 * @param file Fichier dans lequel on sauvegarde
	 * @param obj  Objet a sauvegarder
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void saveObejct(File file, Object obj) throws FileNotFoundException, IOException {
		ObjectOutputStream o = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
		o.writeObject(obj);
		o.close();
	}

	/**
	 * lit un object depuis un fichier
	 * 
	 * @param <T>  Le type de l'objet contenu dans le fichier
	 * 
	 * @param file - le fichier a ouvrir
	 * @return l'objet contenu dans le fichier.
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T loadObject(File file) throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream i = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
		Object tmp = i.readObject();
		i.close();
		return (T) tmp;
	}

	/**
	 * Super Legend Boy
	 */
	public static final String FONT_PIXEL = "/super-legend-boy.otf";
	/**
	 * Digital 7
	 */
	public static final String FONT_NUMERIC = "/digital-7.ttf";

	/**
	 * Charge un font
	 * 
	 * @param name le nom du font
	 * @param size la taille
	 * @return le font
	 */
	public static Font LoadFont(String name, double size) {
		return Font.loadFont(getInputStream(name), size);
	}
	
	public static final String FILE_BN = "/c_batailleNavale";
	public static final String FILE_SU = "/c_sudoku";
	public static final String FILE_PO = "/c_poker";

	public static final String IBN_CONTRE_TORPILLEUR = FILE_BN + "/contretorpilleur.png";
	public static final String IBN_CROISEUR = FILE_BN + "/croiseur.png";
	public static final String IBN_EAU = FILE_BN + "/eau.png";
	public static final String IBN_FEU = FILE_BN + "/feu.gif";
	public static final String IBN_FOND = FILE_BN + "/fond.png";
	public static final String IBN_PORTE_AVION = FILE_BN + "/porteavion.png";
	public static final String IBN_RATE = FILE_BN + "/rate.png";
	public static final String IBN_SOUS_MARIN = FILE_BN + "/sousmarin.png";
	public static final String IBN_TORPILLEUR = FILE_BN + "/torpilleur.png";
	public static final String IBN_TOUCHE = FILE_BN + "/touche.png";
	public static final String IBN_VIDE = FILE_BN + "/vide.png";
	public static final String IBN_TAB = FILE_BN + "/tableau.png";
	public static final String IBN_ALERT = FILE_BN + "/alert_button.png";

	public static final String ISU_TIMER = FILE_SU + "/Timer.gif";
	
	public static final String IBUT_BNS = "/bataille_navale_selected.gif";
	public static final String IBUT_BN = "/bataille_navale.png";
	public static final String IBUT_LOS = "/loto_selected.gif";
	public static final String IBUT_LO = "/loto.png";
	public static final String IBUT_POS = "/poker_selected.gif";
	public static final String IBUT_PO = "/poker.png";
	public static final String IBUT_SCS = "/score_selected.gif";
	public static final String IBUT_SC = "/score.png";
	public static final String IBUT_SUS = "/sudoku_selected.gif";
	public static final String IBUT_SU = "/sudoku.png";
	
	public static final String IPO_CACHER = FILE_PO + "/cacher.png";
	/**
	 * Charge une image grace a son nom.
	 * 
	 * @param name - le nom de l'image
	 * @return une image
	 */
	public static Image loadImage(String name) {
		return load(name, Image.class);
	}

	public static final String MBN_EXPLOSION = FILE_BN + "/explosion.wav";
	public static final String MBN_HEAVY_EXPLOSION = FILE_BN + "/heavyExplosion.wav";
	public static final String MBN_SHOT = FILE_BN + "/shot.wav";
	public static final String MBN_SPLASH = FILE_BN + "/splash.wav";
	public static final String MBN_ALARM = FILE_BN + "/alarm.mp3";
	public static final String MCLICK = "/click.wav";
	
	/**
	 * Charge un Media.
	 * 
	 * @param name - le nom du media
	 * @return le Media.
	 */
	public static Media loadMedia(String name) {
		return new Media(getRessource(name));
	}
}
