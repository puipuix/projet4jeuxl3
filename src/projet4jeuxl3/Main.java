package projet4jeuxl3;

import java.time.LocalTime;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * 
 * @author Alexis
 *
 */
public class Main extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		// new Accueil().show();
		new Accueil().show();
	}

	/**
	 * Main
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	/**
	 * Ecrit un message dans la console avec l'h actuelle et la ligne qui appel cette fonction.
	 * @param o - L'objet ou le message a ecrire.
	 */
	public static void debug(Object o) {

		StackTraceElement e = Thread.currentThread().getStackTrace()[2];

		System.out.println(LocalTime.now() + "(" + e.getFileName() + ":" + e.getLineNumber() + "): " + o);
	}

	/**
	 * Ajoute effet d'ombre bleu a la node lorsqu'elle prend le focus.
	 * 
	 * @param target - La node focusable.
	 */
	public static void ApplyFocusEffect(Node target) {
		target.focusedProperty().addListener(new ChangeListener<Boolean>() {

			Effect defaultEffect;

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue) {
					defaultEffect = target.getEffect();
					DropShadow ds = new DropShadow(3, Color.DEEPSKYBLUE);
					ds.setSpread(.7);
					target.setEffect(ds);
				} else {
					target.setEffect(defaultEffect);
				}
			}
		});
	}
}
