package projet4jeuxl3;

import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Menu de pause
 * 
 * @author Alexis
 *
 */
public class PauseMenuJeux extends PauseMenuBase {

	private Button retour;
	private BoutonFenetreSuivante quitter;

	/**
	 * Cree un menu de pause du jeu
	 * 
	 * @param current - le stage du jeu
	 */
	public PauseMenuJeux(Stage current) {
		super(new VBox());

		// retour a l'accueil
		quitter = new BoutonFenetreSuivante("Quitter", current, () -> {
			setMenuVisible(false);
			return new Accueil();
		});
		quitter.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 50));

		// retour en jeu
		retour = new Button("Retour");
		retour.setOnAction(e -> {
			Hide();
		});
		retour.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER)
				retour.fire();
		});
		retour.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 50));

		menu.getChildren().addAll(quitter, retour);
	}

	@Override
	public void Show() {
		super.Show();
		// focus sur le bouton retour pour enlever celui d'un bouton derriere l'ecran
		retour.requestFocus();
	}

	/**
	 * @return Le bouton retour au jeu
	 */
	public Button getRetour() {
		return retour;
	}

	/**
	 * @return Le bouton pour quitter le jeu
	 */
	public BoutonFenetreSuivante getQuitter() {
		return quitter;
	}

}
