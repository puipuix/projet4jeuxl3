package projet4jeuxl3;

import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.Region;

/**
 * Redimentionne ses fils a la taille de la fenetre si ceux-ci sont des regions.
 * 
 * @author Alexis
 *
 */
public class MultipleRoot extends Region {

	/**
	 * La node principale
	 */
	private Node mainRoot;

	/**
	 * les autres nodes qui seront redimentionne a la taille de la fenetre
	 */
	private Group subRoots;

	/**
	 * Si la node principale doit etre redimentionne
	 */
	private boolean resizeMainRoot;

	/**
	 * Le listener de la node principale
	 */
	private ChangeListener<Scene> mainListener;

	/**
	 * Constructeur par defaut
	 */
	public MultipleRoot() {
		this(new Region[0]);
	}

	/**
	 * Initialise les eventListeners et ajoute les fils a la liste des fils.
	 * 
	 * @param subRoot un ensemble de fils secondaires
	 */
	public MultipleRoot(Region... subRoot) {
		subRoots = new Group();
		this.setBackground(Background.EMPTY);

		// Reagit a chaque ajout dans les la liste des fils
		subRoots.getChildren().addListener(new ListChangeListener<Node>() {

			@Override
			public void onChanged(Change<? extends Node> c) {
				// Pour toutes les nodes ajout�es
				while (c.next()) {
					for (Node node : c.getAddedSubList()) {
						applyListener(node);
					}
				}
			}
		});
		this.getChildren().add(subRoots);
		// on ajoute les fils en entree
		subRoots.getChildren().addAll(subRoot);
	}

	private ChangeListener<Scene> applyListener(Node node) {
		Region that = this; // permet d'appeler this dans les classes annonymes.
		// Si c'est une r�gion (alors elle peut �tre redimentionn�)
		if (node != null && node instanceof Region) {
			Region region = (Region) node; // cast en region

			// Listener recuperer la nouvelle scene si elle change
			ChangeListener<Scene> sceneUpdate = (v, o, scene) -> {
				if (scene != null) {
					// Listener pour mettre a jour la hauteur
					ChangeListener<Number> height = (hv, ho, newValue) -> {

						// force la taille
						region.setMaxHeight(newValue.doubleValue());
						region.setMinHeight(newValue.doubleValue());
						region.setPrefHeight(newValue.doubleValue());
					};
					scene.heightProperty().addListener(height);
					height.changed(scene.heightProperty(), scene.getHeight(), scene.getHeight());

					// Listener pour mettre a jour la largeur
					ChangeListener<Number> witdh = (wv, wo, newValue) -> {
						// force la taille
						region.setMaxWidth(newValue.doubleValue());
						region.setMinWidth(newValue.doubleValue());
						region.setPrefWidth(newValue.doubleValue());
					};
					scene.widthProperty().addListener(witdh);
					witdh.changed(scene.widthProperty(), scene.getWidth(), scene.getWidth());
				}
			};

			// On ajoute le listener a la propriete de la scene du MultipleRoot
			that.sceneProperty().addListener(sceneUpdate);

			// On appel le listener dans le cas ou la scene est deja mise
			sceneUpdate.changed(that.sceneProperty(), that.getScene(), that.getScene());
			return sceneUpdate;
		}
		return null;
	}

	/**
	 * @return La list des nodes secondaires
	 */
	public ObservableList<Node> getSubRoots() {
		return subRoots.getChildren();
	}
	
	/**
	 * @return La node principale
	 */
	public Node getMainRoot() {
		return mainRoot;
	}

	/**
	 * Change la node principale
	 * @param node - la nouvelle node
	 * @param setInBackground - si cette node doit etre place derriere les secondaires ou non
	 */
	public void setMainRoot(Node node, boolean setInBackground) {
		this.getChildren().clear();
		mainRoot = node;

		if (setInBackground) {
			this.getChildren().addAll(mainRoot, subRoots);
		} else {
			this.getChildren().addAll(subRoots, mainRoot);
		}

		if (resizeMainRoot) {
			if (mainListener != null) {
				this.sceneProperty().removeListener(mainListener);
			}
			mainListener = applyListener(mainRoot);
		}

	}

	/**
	 * Change la node principale
	 * @param node - la nouvelle node
	 */
	public void setMainRoot(Node node) {
		setMainRoot(node, true);
	}

	/**
	 * @param v - indique s'il faut redimentionner la node principale
	 */
	public void resizeMainRoot(boolean v) {
		resizeMainRoot = v;
		if (v) {
			if (mainListener == null) {
				mainListener = applyListener(mainRoot);
			}
		} else {
			if (mainListener != null) {
				this.sceneProperty().removeListener(mainListener);
			}
		}
	}
}
