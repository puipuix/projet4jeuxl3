package projet4jeuxl3;

import Bataille_naval.BatailleNavaleStage;
import Loto.StageLoto;
import Sudoku.SudoAff;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import poker.AffichagePoker;

/***
 * 
 * @author Samuel, Alexis
 *
 */
public class Accueil extends Stage {
	static int duree = 100;
	boolean canmove = true;
	int current = 0;

	/**
	 * 
	 */
	public Accueil() {

		VBox root = new VBox();

		Scene scene = new Scene(new Group(root));

		this.setScene(scene);

		Media m = ContentManager.loadMedia(ContentManager.MCLICK);
		MediaPlayer mp = new MediaPlayer(m);

		ImageView[] il = new ImageView[5];

		il[0] = build(ContentManager.IBUT_BNS, ContentManager.IBUT_BN, mp, () -> {
			return new BatailleNavaleStage();
		});
		il[1] = build(ContentManager.IBUT_POS, ContentManager.IBUT_PO, mp, () -> {
			return new AffichagePoker();
		});
		il[2] = build(ContentManager.IBUT_LOS, ContentManager.IBUT_LO, mp, () -> {
			return new StageLoto();
		});
		il[3] = build(ContentManager.IBUT_SUS, ContentManager.IBUT_SU, mp, () -> {
			return new SudoAff();
		});
		il[4] = build(ContentManager.IBUT_SCS, ContentManager.IBUT_SC, mp, () -> {
			return new StageScore();
		});
		for (int i = 0; i < 5; i++) {
			root.getChildren().add(il[i]);
		}

		root.heightProperty().addListener(new ChangeListener<Number>() {
			int step = 0;
			double heightDiff = 0;

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (step == 1) {
					heightDiff = getHeight() - oldValue.doubleValue();
					step = 2;
				}
				if (step == 0) {
					step = 1;
				} else {
					setHeight(newValue.doubleValue() + heightDiff);
				}
				setResizable(false);
				root.requestFocus();
			}

		});
		Platform.runLater(() -> {
			il[0].requestFocus();
		});
	}

	private ImageView build(String s, String us, MediaPlayer click, BuildStageEventHandler toOpen) {
		final Image selected = ContentManager.loadImage(s), unselected = ContentManager.loadImage(us);

		ImageView view = new ImageView(unselected);
		view.setFocusTraversable(true);
		view.focusedProperty().addListener((v, o, n) -> {
			if (n) {
				click.stop();
				click.play();
				view.setImage(selected);
			} else {
				view.setImage(unselected);
			}
		});
		view.setOnMouseMoved(e -> {
			if (!view.isFocused()) {
				view.requestFocus();
			}
		});
		view.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				toOpen.Build().show();
				this.close();
			}
		});
		view.setOnMouseClicked(e -> {
			toOpen.Build().show();
			this.close();
		});

		return view;
	}
}
