package projet4jeuxl3;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Menu lors de la fin d'une partie
 * 
 * @author Alexis
 *
 */
public class PauseMenuVictoireDefaite extends PauseMenuBase {

	Label label;
	Label subTitle;
	Label scorePts;
	Label scoreTitle;
	BoutonFenetreSuivante quitter;

	/**
	 * @param current - le stage du jeu
	 * @param next    - un constructeur du stage du jeu pour recommencer une partie
	 */
	public PauseMenuVictoireDefaite(Stage current, BuildStageEventHandler next) {
		super(new VBox());

		// titre victoire / defaite
		label = new Label("D�faite");
		label.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 50));
		label.setTextFill(Color.WHITE);

		// sous titre facultatif
		subTitle = new Label("");
		subTitle.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 30));
		subTitle.setTextFill(Color.WHITE);

		scorePts = new Label("+0");
		scorePts.setFont(ContentManager.LoadFont(ContentManager.FONT_NUMERIC, 30));
		scorePts.setTextFill(Color.WHITE);

		scoreTitle = new Label("pts");
		scoreTitle.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 22));
		scoreTitle.setTextFill(Color.WHITE);

		HBox scores = new HBox(0, scorePts, scoreTitle);
		scores.setAlignment(Pos.CENTER);

		// retour a l'accueil
		quitter = new BoutonFenetreSuivante("Quitter", current, () -> {
			setMenuVisible(false);
			return new Accueil();
		});
		quitter.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 50));

		// Relance la partie en recreant le jeu
		BoutonFenetreSuivante recommencer = new BoutonFenetreSuivante("Recommencer", current, () -> {
			setMenuVisible(false);
			return next.Build();
		});
		recommencer.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 50));

		menu.getChildren().addAll(label, subTitle, scores, recommencer, quitter);
	}

	/**
	 * Met le titre a victoire ou defaite
	 * 
	 * @param victory
	 */
	public void setVictory(boolean victory) {
		if (victory) {
			label.setText("Victoire");
		} else {
			label.setText("D�faite");
		}
	}

	/**
	 * Ecrit le sous titre
	 * 
	 * @param text
	 */
	public void setSubTitle(String text) {
		subTitle.setText(text);
	}

	/**
	 * Ecrit le score
	 * 
	 * @param score
	 */
	public void setScore(long score) {
		scorePts.setText("+" + score);
	}

	@Override
	public void Show() {
		super.Show();
		// focus sur le bouton retour pour enlever celui d'un bouton derriere l'ecran
		quitter.requestFocus();
	}
}
