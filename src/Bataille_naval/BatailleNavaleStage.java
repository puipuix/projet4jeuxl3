package Bataille_naval;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import projet4jeuxl3.MultipleRoot;
import projet4jeuxl3.PauseMenuJeux;
import projet4jeuxl3.PauseMenuVictoireDefaite;

/***
 * 
 * @author samuel
 *
 */
public class BatailleNavaleStage extends Stage {

	private BorderPane root;
	private Scene scene;
	private BNPlayer player1;
	public Stage stage;
	public PauseMenuVictoireDefaite fin;
	public PauseMenuJeux menujeu;

	/**
	 * initie le lancement du jeu 
	 * @param root
	 * @throws InstantiationException
	 */
	public void lancement(BorderPane root) throws InstantiationException {

		BNPlayer player = new BNPlayer(new BNPlayer_IA(this));
		player.initGrid(this);

	}

	public Pane getRoot() {
		return root;
	}

	public BatailleNavaleStage() {

		root = new BorderPane();
		MultipleRoot rootFinPause = new MultipleRoot();// cr�ation des volets pause et fin
		rootFinPause.resizeMainRoot(true);
		rootFinPause.setMainRoot(root);

		scene = new Scene(rootFinPause, 1200, 600);
		this.setScene(scene);
		root.setBackground(new Background(new BackgroundFill(Color.rgb(22, 22, 22), null, null)));

		this.setResizable(false);

		root.requestFocus();
		scene.setOnKeyPressed(e -> {// permet d'afficher le menu pause
			if (e.getCode() == KeyCode.ESCAPE) {
				menujeu.Show();
			}
		});

		menujeu = new PauseMenuJeux(this);

		fin = new PauseMenuVictoireDefaite(this, () -> {
			return new BatailleNavaleStage();
		});
		rootFinPause.getSubRoots().addAll(menujeu, fin);
		root.setOnKeyPressed(e -> {
			//System.out.println("in");
			if (e.getCode() == KeyCode.ESCAPE) {
				if (menujeu.isVisible()) {
					//System.out.println("etait visible");
					menujeu.Hide();
				} else {
					//System.out.println("etait pas visible");
					menujeu.Show();
				}
			}
		});

		try {
			lancement(root);
		} catch (InstantiationException e) {
			e.printStackTrace();
		}

	}

	/**
	 * permet de lancer la partie
	 */
	public void launchGame() {

		player1 = BNPlayer.player;

		root.getChildren().clear();

		root.setLeft(player1.g.getBoat());
		Group tab = player1.getGrille().board;
		tab.setTranslateX(500);
		root.getChildren().add(tab);
		player1.getGrille().updateBoard("Pr�t �\nfaire feu", "");

		root.setRight(player1.g.getTarget());

	}

}
