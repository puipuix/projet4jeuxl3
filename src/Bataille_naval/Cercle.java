package Bataille_naval;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

public class Cercle extends Circle {
	Color baseColor,alertColor;
	Timeline alertTl;
	
	public Cercle() {
		super();
	}
	
	/**
	 * cree un cercle
	 * @param taille rayon du cercle
	 * @param baseColor couleur en mode off
	 * @param alertColor couleur en mode on 
	 */
	public Cercle(int taille,Color baseColor,Color alertColor) {
		super(taille,baseColor);
		this.baseColor=baseColor;
		this.alertColor=alertColor;
		alertTl=new Timeline();
		alertTl.getKeyFrames().add(new KeyFrame(Duration.ZERO, e->{setFill(baseColor);}));
		alertTl.getKeyFrames().add(new KeyFrame(Duration.seconds(1), e->{setFill(alertColor);}));
		alertTl.setAutoReverse(true);
		alertTl.setCycleCount(Timeline.INDEFINITE);
	}
	
	/**
	 * lance l'animation
	 */
	public void alertMode() {
		alertTl.play();
	}
	
	/**
	 * passe en mode on
	 */
	public void onMode() {
		alertTl.stop();
		setFill(alertColor);
		
	}
	
	/**
	 * passe en mode off
	 */
	public void offMode() {
		onMode();
		setFill(baseColor);
		
	}

}
