package Bataille_naval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import javafx.util.Pair;
import projet4jeuxl3.ContentManager;

/***
 * 
 * @author samuel
 *
 */
public class BNPlayer_IA extends BNPlayer {

	ArrayList<Pair<Integer, Integer>> caseCible;// tableau avec toutes les coordonnees de tire possible au moment T

	MediaPlayer shot = new MediaPlayer(ContentManager.loadMedia(ContentManager.MBN_SHOT));
	MediaPlayer explosion = new MediaPlayer(ContentManager.loadMedia(ContentManager.MBN_EXPLOSION));
	MediaPlayer plouf = new MediaPlayer(ContentManager.loadMedia(ContentManager.MBN_SPLASH));

	int tour = 0;

	public BNPlayer_IA(BatailleNavaleStage bp) {
		origin = bp;
		initGrid();
		caseCible = new ArrayList<>();
		for (int i = 0; i < 10; i++) {// initialisation des cases a viser
			for (int j = 0; j < 10; j++) {
				caseCible.add(new Pair<Integer, Integer>(i, j));
			}
		}

		Collections.shuffle(caseCible);// m�lange des cases cibles
		Collections.shuffle(caseCible);
		Collections.shuffle(caseCible);

	}

	public void setGrille(Grille g) {
		this.g = g;
	}

	@Override
	public boolean increaseTouche(Boat t) {
		g.scoreUp();
		vie--;
		if (vie == 0) {
			player.fin = true;
			player.sendScore();
			origin.fin.setVictory(true);
			origin.fin.setScore(player.score);
			origin.fin.Show();
		}

		return decreaseBoatLife(t, 1);
	}

	@Override
	public void myTurn() {
		if (!setLastCoup) {// permet un changement de cible
			dernierCoup = nextCible.get(0).getValue();
			setLastCoup = true;
		}
		tour++;

		if (!player.fin) {

			if (nextCible.size() > 0) {// si il y a un bateau a viser on appele l'ia
				ia();
			} else {// sinon tire aleatoire
				Collections.shuffle(caseCible);
				Pair<Integer, Integer> zone = caseCible.get(0);
				tirer(zone.getKey(), zone.getValue());
			}
		}
	}

	/**
	 * initialise la grille
	 */
	public void initGrid() {

		myGrid = new Boat[10][10];
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				myGrid[i][j] = Boat.EAU;// place de l'eau dans toute la grille
			}
		}
//placement des bateaux
		placementBateau(Boat.PORTEAVION);
		placementBateau(Boat.CROISEUR);
		placementBateau(Boat.CONTRETORPILLEUR);
		placementBateau(Boat.SOUSMARIN);
		placementBateau(Boat.SOUSMARIN2);
		placementBateau(Boat.TORPILLEUR);

	}

	/**
	 * permet de trouver une place pour le bateau pass� en parametre et de le
	 * placer
	 * 
	 * @param t bateau que l'on souhaite placer
	 */
	private void placementBateau(Boat t) {
		boolean position = false;
		int i, j, orientation;
		i = j = orientation = 0;
		Random rdm = new Random();

		while (!position) {// on cherche une position libre
			i = rdm.nextInt(10);
			j = rdm.nextInt(10);
			orientation = rdm.nextInt(2);
			position = checkPlacement(t, i, j, orientation);

		}
		myGrid = t.placement(myGrid, i, j, orientation, listeBateau);

	}

	/**
	 * Partie IA
	 */

	private ArrayList<Pair<Boat, Pair<Integer, Integer>>> nextCible = new ArrayList<>();
	private int direction; // 0=droite , 1=haut , 2=gauche , 3=bas. coorespond a la direction d'un bateau
							// cible
	private boolean dernierCoupTouche = false;
	private Pair<Integer, Integer> dernierCoup = null;// coordonnees du dernier coup
	private int ePorteAvion = 5, eCroisseur = 4, eContre = 3, eS1 = 3, eS2 = 3, eTorpilleur = 2; // vie des bateaux
																									// ennemies
	boolean setLastCoup = true;// boolean autorisant le changement de valeur du dernier coup

	/**
	 * demande a l'ia de tirer
	 */
	public void ia() {
//si le dernier coup a touche et que toute les direction n'ont pas etait testee
		if (dernierCoupTouche && direction < 4) {
			int x, y;
			switch (direction) {
			case 0:
				x = dernierCoup.getKey() + 1;
				y = dernierCoup.getValue();
				break;
			case 1:
				x = dernierCoup.getKey();
				y = dernierCoup.getValue() - 1;
				break;
			case 2:
				x = dernierCoup.getKey() - 1;
				y = dernierCoup.getValue();
				break;
			case 3:
				x = dernierCoup.getKey();
				y = dernierCoup.getValue() + 1;
				break;
			default:
				x = 0;
				y = 0;
				break;
			}

			// si la case n'est pas valide
			if (x >= 10 || y >= 10 || x < 0 || y < 0 || cible[x][y] != Boat.VIDE) {

				// on remet le dernier tir a l'origine (premier tir qui a touche le bateau
				// cible)
				dernierCoup = nextCible.get(0).getValue();
				// on change la direction
				direction += 1;
				ia();// on recommence la methode

			} else {// si la case est valide on tire
				if (tirer(x, y)) {
					updateMortEnnemie();// si le tire a touche on met a jour nos cibles

				}

				dernierCoup = new Pair<Integer, Integer>(x, y);// parametrage du dernier coup

			}
		} else if (direction < 4) {// si le dernier tir n'a pas touch� on change de direction et on remet a
									// l'origine

			// si le coup different de l'origine on tire a l'oppose
			if ((nextCible.get(0).getValue().getKey() + 2 <= dernierCoup.getKey()
					|| nextCible.get(0).getValue().getKey() - 2 >= dernierCoup.getKey()
					|| nextCible.get(0).getValue().getValue() + 2 <= dernierCoup.getValue()
					|| nextCible.get(0).getValue().getValue() - 2 >= dernierCoup.getValue()) && direction < 2) {
				// verification que l'on peut tirer a l'opposee et que les raisons de le fare
				// sont valides
				direction += 2;// augmente la direction de 2
				dernierCoup = nextCible.get(0).getValue();// on remet le dernier coup a l'origine du bateau
				dernierCoupTouche = true;// on remet a true pour repartir dans les conditions du debut
				ia();// on rappele la methode
			} else {// sinon on change de direction
				dernierCoupTouche = true;
				direction++;
				dernierCoup = nextCible.get(0).getValue();
				ia();
			}

		} else {// si un probleme apparait on r�initialise les cibles en memoire
			nextCible = new ArrayList<>();
			myTurn();// on recommence le tour

		}
	}

	/**
	 * permet de mettre a jour la vie des bateau ennemies
	 */
	public void updateMortEnnemie() {

		int vieRestante = 0;
		switch (nextCible.get(0).getKey()) {// on cherche quel bateau a etait touche

		case CONTRETORPILLEUR:
			eContre--;// chaque case decremente la vie du bateau touche et parametre la valeur
						// vieRestante
			vieRestante = eContre;
			break;
		case CROISEUR:
			eCroisseur--;
			vieRestante = eCroisseur;
			break;
		case PORTEAVION:
			ePorteAvion--;
			vieRestante = ePorteAvion;
			break;
		case SOUSMARIN:
			eS1--;
			vieRestante = eS1;
			break;
		case SOUSMARIN2:
			eS2--;
			vieRestante = eS2;
			break;
		case TORPILLEUR:
			eTorpilleur--;
			vieRestante = eTorpilleur;
			break;
		default:
			break;
		}

		if (vieRestante == 0) {// si le bateau est mort on le retire de la liste des cible
			nextCible.remove(0);
			if (nextCible.size() > 0) {// si il y a d'autre cible en memoire on les vises
				dernierCoup = nextCible.get(0).getValue();
				setLastCoup = false;// empeche l'ia de changer d'etat
				dernierCoupTouche = true;

			}
			direction = 0;// reinitialisation de la direction des tires

		}
	}

	/**
	 * permet de tirer en X Y
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean tirer(int x, int y) {

		boolean touche = false;

		if (g.playAnimation) {// si les animations sont autoris�es
			Timeline tl = new Timeline();

			tl.getKeyFrames().add(new KeyFrame(Duration.millis(200), e -> {
				explosion.stop();// reinitialise le son explosion
				shot.play();// lance le son d'un tir
			}));

			tl.getKeyFrames().add(new KeyFrame(Duration.seconds(2), e -> {
				plouf.stop();// reinitialise le son d'un coup rate
			}));

			tl.getKeyFrames().add(new KeyFrame(Duration.seconds(3), e -> {
				shot.stop();// reinitialise le son d'un tir
				if (player.myGrid[x][y] == Boat.EAU) {// si la case est vide
					cible[x][y] = Boat.RATE;// on met a jour le tableau des cibles
					plouf.play();// on joue le son d'un coup rate
					try {
						g.updateGrid(null);// on met a jour l'affichage
					} catch (InstantiationException e1) {
						e1.printStackTrace();
					}

				} else {// si il y a un bateau sur la case
					cible[x][y] = Boat.TOUCHE;// on met a jour le tableau des cibles
					explosion.play();// on oue le son d'un coup reussi
					player.touche(player.myGrid[x][y]);// on met a jour le joueur adverse
				}

			}));
			tl.getKeyFrames().add(new KeyFrame(Duration.seconds(4), e -> {
				player.myTurn();// on lance le tour du joueur adverse

			}));
			tl.play();// on joue l'animation

		} else {// si les animations ne sont pas autoris�es on fait le meme deroulement qu'avant
				// sans les sons et le temps d'attente
			if (player.myGrid[x][y] == Boat.EAU) {
				cible[x][y] = Boat.RATE;
				plouf.play();
				try {
					g.updateGrid(null);
				} catch (InstantiationException e1) {
					e1.printStackTrace();
				}

			} else {
				cible[x][y] = Boat.TOUCHE;
				explosion.play();
				player.touche(player.myGrid[x][y]);
			}
			player.myTurn();
		}

		if (player.myGrid[x][y] != Boat.EAU) {//si on a touche un bateau 
			touche = true;
			if (nextCible.size() > 0) {//si il y a deja des cibles 
				boolean canAdd = true;
				for (Pair<Boat, ?> p : nextCible) {//on verifie que le bateau n'est pas deja present
					if (p.getKey().name() == player.myGrid[x][y].name())
						canAdd = false;
				}

				if (canAdd) {
					nextCible.add(0, new Pair<Boat, Pair<Integer, Integer>>(player.myGrid[x][y],
							new Pair<Integer, Integer>(x, y)));//ajout de la cible
					direction = 0;

				}
			} else {//sinon on ajoute la cible

				nextCible.add(
						new Pair<Boat, Pair<Integer, Integer>>(player.myGrid[x][y], new Pair<Integer, Integer>(x, y)));

				updateMortEnnemie();

				dernierCoup = new Pair<Integer, Integer>(x, y);
			}
		} else {

			dernierCoup = new Pair<Integer, Integer>(x, y);
		}

		caseCible.remove(new Pair<Integer, Integer>(x, y));//on retire la case qui vient d'etre visee

		dernierCoupTouche = touche;
		return touche;
	}

}
