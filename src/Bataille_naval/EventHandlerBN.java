package Bataille_naval;


import javafx.scene.image.ImageView;

/***
 * 
 * @author samuel
 *
 */

abstract public class EventHandlerBN{
	
	public abstract void handle(ImageView iv,int i,int j,int orientation);
}
