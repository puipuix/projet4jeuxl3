package Bataille_naval;

import javafx.scene.image.Image;
import projet4jeuxl3.ContentManager;

/***
 * 
 * @author samuel
 *
 */
public enum Boat {

	SOUSMARIN2(3), SOUSMARIN(3), TORPILLEUR(2), CONTRETORPILLEUR(3), CROISEUR(4), PORTEAVION(5), EAU(1), RATE(1),
	TOUCHE(1), VIDE(1);
	static int tailleCase = 36;//taille d'une case dans la grille

	static int rangTableau = 0; 
	public int nbCase;

	Boat(int i) {
		nbCase = i;
	}
	
/**
 * 
 * @param tab tableau ou l'on place le bateau
 * @param x coordonnee
 * @param y coordonnee
 * @param orientation orientation du bateau
 * @param listeBateau tableau de sauvegarde
 * @return
 */
	public Boat[][] placement(Boat[][] tab, int x, int y, int orientation, Couple[] listeBateau) { 
		if (orientation == 0) {
			for (int i = x; i < nbCase + x; i++) {
				tab[i][y] = this;
			}
			listeBateau[rangTableau] = new Couple(this, x, y, true);
		} else {
			for (int i = y; i > y - nbCase; i--) {
				tab[x][i] = this;
			}

			listeBateau[rangTableau] = new Couple(this, x, y - (nbCase - 1), false);
		}
		rangTableau++;
		if (rangTableau >= 6) {
			rangTableau -= 6;
		}
		return tab;
	}
	
	/**
	 * retourne l'image du bateau actuel
	 * @return
	 * @throws InstantiationException
	 */
	public Image getImage() throws InstantiationException {
		switch(this) {
		case PORTEAVION: return ContentManager.loadImage(ContentManager.IBN_PORTE_AVION);
		case CROISEUR:return ContentManager.loadImage(ContentManager.IBN_CROISEUR);
		case SOUSMARIN:case SOUSMARIN2:return ContentManager.loadImage(ContentManager.IBN_SOUS_MARIN);
		case CONTRETORPILLEUR: return ContentManager.loadImage(ContentManager.IBN_CONTRE_TORPILLEUR);
		case TORPILLEUR:return ContentManager.loadImage(ContentManager.IBN_TORPILLEUR);
		case RATE:return ContentManager.loadImage(ContentManager.IBN_RATE);
		case VIDE:return ContentManager.loadImage(ContentManager.IBN_VIDE);
		case TOUCHE:return ContentManager.loadImage(ContentManager.IBN_TOUCHE);
		case EAU:return ContentManager.loadImage(ContentManager.IBN_EAU);
		default :return null;
		
		}
	}

	/**
	 * retourne la taille du bateau actuel
	 * @return
	 */
	public int getHeight() {
return nbCase*tailleCase;
		/*switch (this) {
		case PORTEAVION:
			return tailleCase * 5;
		case CROISEUR:
			return tailleCase * 4;
		case SOUSMARIN:
		case SOUSMARIN2:
			return tailleCase * 3;
		case CONTRETORPILLEUR:
			return tailleCase * 3;
		case TORPILLEUR:
			return tailleCase * 2;
		default:
			return 0;
		}*/

	}

	/**
	 * affiche la grille de bateau t dans la console 
	 * @param t
	 */
	@Deprecated
	public void affichageGrille(Boat[][] t) {
		String s = "";
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				s += "\t " + t[j][i].name().charAt(0);
			}
			s += "\n";
		}

		System.out.println(s);
	}
	
	

}

class Couple {
	Boat type;
	int coorX, coorY;
	boolean horizontal;

	public Couple(Boat t, int x, int y, boolean h) {
		type = t;
		coorX = x;
		coorY = y;
		horizontal = h;
	}

}
