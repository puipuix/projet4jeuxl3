package Bataille_naval;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import projet4jeuxl3.ContentManager;

/***
 * 
 * @author samuel
 *
 */
public class Grille {

	//public Boat[][] tab;
	public BNPlayer player;
	public BNPlayer ia;
	public Group boat, target, board;
	public int decalage;
	public Image fond;
	public int tailleCase;
	Label textHaut, textBas, score, animation;//label affichage du milieu
	public String coule = null; // string contenant le bateau precedement coule

	MediaPlayer shot = new MediaPlayer(ContentManager.loadMedia(ContentManager.MBN_SHOT));
	MediaPlayer explosion = new MediaPlayer(ContentManager.loadMedia(ContentManager.MBN_EXPLOSION));
	Timeline tl, tlAlert, increaseScore, decreaseScore;
	ImageView alertButton;
	MediaPlayer alert = new MediaPlayer(ContentManager.loadMedia(ContentManager.MBN_ALARM));
	MediaPlayer plouf = new MediaPlayer(ContentManager.loadMedia(ContentManager.MBN_SPLASH));
	Cercle[] boatPlayerWarning, boatIaWarning;
	GridPane gp;

	HBox visibleBoat,playAnimationHBox;
	CheckBox visibleBoatCheckBox,playAnimationCheckBox;
	Label visibleBoatLabel,playAnimationLabel;
	boolean playAnimation=false;
	
	public Grille(BNPlayer p, BNPlayer_IA p2, int d, Image f, int tc) {
		player = p;
		boat = new Group();
		target = new Group();
		fond = f;
		decalage = d;
		tailleCase = tc;
		ia = p2;

		boatIaWarning = new Cercle[6];
		boatPlayerWarning = new Cercle[6];
		for (int i = 0; i < 6; i++) {

			boatIaWarning[i] = new Cercle(6, Color.rgb(255, 72, 72), Color.rgb(56, 5, 5));
			boatIaWarning[i].setStroke(Color.BLACK);
			boatPlayerWarning[i] = new Cercle(6, Color.rgb(255, 72, 72), Color.rgb(56, 5, 5));
			boatPlayerWarning[i].setStroke(Color.BLACK);
		}
		createBoard();
	}

	public Group getBoat() {
		return boat;
	}

	public Group getTarget() {
		return target;
	}

	/**
	 * demande la mise a jour des grilles
	 * @param event methode a executer sur la grille du joueur 
	 */
	public void update(EventHandlerBN event) {
		try {
			updateGrid(event);
			updateCible();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * permet la creation du tableau du milieu
	 */
	private void createBoard() {
		ImageView iv = new ImageView(ContentManager.loadImage(ContentManager.IBN_TAB));
		alertButton = new ImageView(ContentManager.loadImage(ContentManager.IBN_ALERT));
		alertButton.setTranslateY(130);
		alertButton.setTranslateX(66);
		alertButton.setVisible(false);

		textHaut = new Label("Placer vos\nbateaux");
		textBas = new Label();
		textHaut.setMaxHeight(53);
		textHaut.setMinHeight(53);
		textHaut.setPrefHeight(53);
		textBas.setMaxHeight(53);
		textBas.setMinHeight(53);
		textBas.setPrefHeight(53);

		/*
		 * textHaut.setMaxWidth(Double.MAX_VALUE); textHaut.setMinWidth(147);
		 * textHaut.setMaxWidth(Double.MAX_VALUE);
		 * textBas.setMaxWidth(Double.MAX_VALUE); textBas.setMinWidth(147);
		 * textBas.setMaxWidth(Double.MAX_VALUE);
		 */

		textHaut.setTextAlignment(TextAlignment.CENTER);
		textBas.setTextAlignment(TextAlignment.CENTER);

		textHaut.setTranslateX(27);
		textHaut.setTranslateY(54);
		textBas.setTranslateX(28);
		textBas.setTranslateY(213);

		textHaut.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 14));
		textBas.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 12));
		System.out.println(textHaut.getHeight() + " " + textHaut.getWidth());

		textHaut.widthProperty().addListener((v, o, n) -> {

			double largeur = n.doubleValue();

			textHaut.setTranslateX(27 + ((147 - largeur) / 2));

		});

		textBas.widthProperty().addListener((v, o, n) -> {

			double largeur = n.doubleValue();

			textBas.setTranslateX(28 + ((147 - largeur) / 2));

			if (largeur > 140) {
				textBas.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 9));
			} else {
				textBas.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 9));
			}

		});

		board = new Group(iv, textHaut, textBas, alertButton);
		tlAlert = new Timeline();
		tlAlert.getKeyFrames().addAll(new KeyFrame(Duration.ZERO, e -> {
			alertButton.setVisible(true);
			alert.play();
		}), new KeyFrame(Duration.seconds(1), e -> {
			alertButton.setVisible(false);
		}), new KeyFrame(Duration.seconds(2), e -> {
			alertButton.setVisible(true);
		}), new KeyFrame(Duration.seconds(3), e -> {
			alertButton.setVisible(false);
		}), new KeyFrame(Duration.seconds(4), e -> {
			alertButton.setVisible(true);
		}), new KeyFrame(Duration.seconds(5), e -> {
			alertButton.setVisible(false);
			alert.stop();
		}));

		createBoard2();

		//gestion de l'affichage du score
		score = new Label("Score :" + player.score + "/20000");
		score.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 12));
		score.setTextFill(Color.WHITE);
		board.getChildren().add(score);
		score.setTranslateY(10);
		score.widthProperty().addListener((a, b, c) -> {

			score.setTranslateX((200 - c.doubleValue()) / 2);

		});

		animation = new Label();
		animation.setTranslateX(90);
		animation.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 12));
		animation.setVisible(false);
		board.getChildren().add(animation);

	
		increaseScore = new Timeline();
		increaseScore.getKeyFrames().addAll(new KeyFrame(Duration.ZERO, e -> {
			animation.setText("+500pts");
			animation.setTextFill(Color.LIMEGREEN);
			animation.setVisible(true);

			animation.setTranslateY(10);
		}), new KeyFrame(Duration.millis(800), new KeyValue(animation.translateYProperty(), 10 + 40)),
				new KeyFrame(Duration.millis(800), e -> {
					animation.setVisible(false);
					animation.setTranslateY(10);
				}));
		increaseScore.setCycleCount(1);
		decreaseScore = new Timeline();
		decreaseScore.getKeyFrames().addAll(new KeyFrame(Duration.ZERO, e -> {
			animation.setText("-500pts");
			animation.setTextFill(Color.RED);
			animation.setVisible(true);
			animation.setTranslateY(10);
		}), new KeyFrame(Duration.millis(800), new KeyValue(animation.translateYProperty(), 10 + 40)),
				new KeyFrame(Duration.millis(800), e -> {
					animation.setVisible(false);
					animation.setTranslateY(10);
				}));
		decreaseScore.setCycleCount(1);

		//gestion de l'affichage des parametres du jeu 
		visibleBoat = new HBox();
		visibleBoatCheckBox = new CheckBox();
		visibleBoatCheckBox.setOnAction(e -> {

			if (visibleBoatCheckBox.isSelected()) {
				try {
					addBoat(target, ia, 0.5);
				} catch (InstantiationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				try {
					updateCible();
				} catch (InstantiationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		visibleBoatLabel = new Label("Bateaux ennemies visibles");
		visibleBoatLabel.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 9));
		visibleBoatLabel.setTextFill(Color.WHITE);
		visibleBoat.getChildren().addAll(visibleBoatCheckBox, visibleBoatLabel);
		visibleBoat.setTranslateY(273);
		visibleBoat.setTranslateX(10);
		board.getChildren().add(visibleBoat);
		
		playAnimationHBox = new HBox();
		playAnimationCheckBox = new CheckBox();
		playAnimationLabel = new Label("Jouer les animations");
		playAnimationLabel.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 9));
		playAnimationLabel.setTextFill(Color.WHITE);
		playAnimationHBox.getChildren().addAll(playAnimationCheckBox, playAnimationLabel);
		playAnimationHBox.setTranslateY(293);
		playAnimationHBox.setTranslateX(10);
		board.getChildren().add(playAnimationHBox);
		playAnimationCheckBox.selectedProperty().addListener((e,o,n)->{
			playAnimation=n;
		});
		
		playAnimationCheckBox.setSelected(true);
	}

	/**
	 * creation du tableau recapitulatif de la vie des bateaux ennemies 
	 * @return
	 */
	public GridPane createBoard2() {
		gp = new GridPane();
		gp = warningBoardBuilder(0, "Porte Avion", gp);
		gp = warningBoardBuilder(1, "Croiseur", gp);
		gp = warningBoardBuilder(2, "Contre torpilleur", gp);
		gp = warningBoardBuilder(3, "Sous marin", gp);
		gp = warningBoardBuilder(4, "Sous marin", gp);
		gp = warningBoardBuilder(5, "Torpilleur", gp);

		board.getChildren().add(gp);

		gp.widthProperty().addListener((a, b, c) -> {
			gp.setTranslateX((200 - c.doubleValue()) / 2);
		});

		gp.setTranslateY(345);
		return gp;
	}

	/**
	 * permet de placer les warning 
	 * @param rang rang auquel placer l'affichage
	 * @param boatName nom du bateau a placer
	 * @param gp gridpane dans le quel placer l'affichage
	 * @return
	 */
	public GridPane warningBoardBuilder(int rang, String boatName, GridPane gp) {
		gp.add(boatPlayerWarning[rang], 0, rang);
		Label l = new Label(boatName);
		l.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 10));
		l.setTextFill(Color.WHITE);
		l.setPadding(new Insets(5, 10, 5, 10));
		GridPane.setHalignment(l, HPos.CENTER);
		gp.add(l, 1, rang);
		gp.add(boatIaWarning[rang], 2, rang);
		return gp;
	}

	/**
	 * met a jour l'affichage du milieu
	 * 
	 * si un parametre est a null l'affichage de ce dernier ne changera pas
	 * 
	 * @param haut texte du haut
	 * @param bas texte du bas 
	 */
	public void updateBoard(String haut, String bas) {
		if (haut != null)
			textHaut.setText(haut);
		if (bas != null && coule == null) {
			textBas.setText(bas);
		} else if (bas != null) {
			textBas.setText(coule);
			coule = null;
		}

	}

	/**
	 * lance l'animation d'alerte
	 */
	public void Alert() {
		if(playAnimation)
		tlAlert.play();
	}

	/**
	 * 
	 * @param t      = le bateau en danger
	 * @param joueur = 0-> joueur 1-> ia
	 * @param state  = 0-> danger 1-> mort
	 */
	public void boatStateChange(Boat t, int joueur, int state) {
		int rang = 0;
		String boat = t.name();
		switch (t) {
		case PORTEAVION:
			rang = 0;
			break;
		case CONTRETORPILLEUR:
			rang = 2;
			break;
		case CROISEUR:
			rang = 1;
			break;
		case SOUSMARIN:
			boat = "sous marin";
			rang = 3;
			break;
		case SOUSMARIN2:
			boat = "sous marin";
			rang = 4;
			break;
		case TORPILLEUR:
			rang = 5;
			break;
		default:
			rang = -1;
			break;

		}
		if (rang != -1) {
			if (joueur == 0) {
				if (state == 0) {
					boatPlayerWarning[rang].alertMode();
				} else {
					boatPlayerWarning[rang].onMode();

				}
			} else {
				if (state == 0) {
					boatIaWarning[rang].alertMode();
				} else {
					boatIaWarning[rang].onMode();

					coule = boat + "\ncoul�";
				}
			}

		}
	}

	/**
	 * met a jour le tableau droit 
	 * @throws InstantiationException
	 */
	private void updateCible() throws InstantiationException {
		target.getChildren().clear();
		target.getChildren().add(new ImageView(fond));
		int x = decalage, y = decalage;

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {

				if (player.cible[i][j] == Boat.VIDE) {

					Rectangle r = new Rectangle(36, 36, Color.TRANSPARENT);
					r.setStroke(Color.GREEN);

					r.setTranslateX(x);
					r.setTranslateY(y);

					target.getChildren().add(r);

					int tmp = i;
					int tmp2 = j;
					int xtmp = x;
					int ytmp = y;
					if (!player.fin) {
						r.setOnMouseEntered(e -> {
							if (player.myTurn)
								r.setFill(Color.GREEN);
						});
						r.setOnMouseExited(e -> {
							if (player.myTurn)
								r.setFill(Color.TRANSPARENT);
						});

						r.setOnMouseClicked(e -> {//parametre l'action a faire si une case est clique
							if (player.myTurn) {
								if(playAnimation) {
									r.setFill(Color.GREEN);
								tl = new Timeline();
								tl.getKeyFrames().addAll(new KeyFrame(Duration.ZERO, t -> {
									updateBoard("impact dans\n3", "");
									player.myTurn = false;
									explosion.stop();
									shot.play();
								}), new KeyFrame(Duration.seconds(3), t -> {
									shot.stop();
									if (ia.myGrid[tmp][tmp2] != Boat.EAU) {
										explosion.play();
										player.cible[tmp][tmp2] = Boat.TOUCHE;
										player.ia.increaseTouche(ia.myGrid[tmp][tmp2]);
										updateBoard("Rechargement", ia.myGrid[tmp][tmp2].name() + "\n touch� !");

									} else {
										updateBoard("Rechargement", "tire rat�");
										plouf.play();
										player.cible[tmp][tmp2] = Boat.RATE;
									}
									try {
										updateCible();
									} catch (InstantiationException e1) {
										e1.printStackTrace();
									}
								}), new KeyFrame(Duration.seconds(4), t -> {
									player.setTurnFalse();
								}), new KeyFrame(Duration.ZERO, new KeyValue(r.widthProperty(), 36)),
										new KeyFrame(Duration.ZERO, new KeyValue(r.heightProperty(), 36)),
										new KeyFrame(Duration.ZERO, new KeyValue(r.translateXProperty(), xtmp)),
										new KeyFrame(Duration.ZERO, new KeyValue(r.translateYProperty(), ytmp)),
										new KeyFrame(Duration.seconds(3), new KeyValue(r.widthProperty(), 0)),
										new KeyFrame(Duration.seconds(3), new KeyValue(r.heightProperty(), 0)),
										new KeyFrame(Duration.seconds(3),
												new KeyValue(r.translateXProperty(), xtmp + 18)),
										new KeyFrame(Duration.seconds(3),
												new KeyValue(r.translateYProperty(), ytmp + 18)),

										new KeyFrame(Duration.seconds(1), t -> {
											updateBoard("impact dans\n2", null);
										}), new KeyFrame(Duration.seconds(2), t -> {
											plouf.stop();
											updateBoard("impact dans\n1", null);
										})

								);
								tl.play();
								}else {
									if (ia.myGrid[tmp][tmp2] != Boat.EAU) {										
										player.cible[tmp][tmp2] = Boat.TOUCHE;
										player.ia.increaseTouche(ia.myGrid[tmp][tmp2]);
										updateBoard("Rechargement", ia.myGrid[tmp][tmp2].name() + "\n touch� !");

									} else {
										updateBoard("Rechargement", "tire rat�");
										player.cible[tmp][tmp2] = Boat.RATE;
									}
									try {
										updateCible();
									} catch (InstantiationException e1) {
										e1.printStackTrace();
									}
									
									player.setTurnFalse();
								}
								
							}

						});
					}

				} else {
					ImageView iv = new ImageView(player.cible[i][j].getImage());
					iv.setTranslateX(x);
					iv.setTranslateY(y);

					target.getChildren().add(iv);
				}

				y += tailleCase;
			}
			x += tailleCase;
			y = decalage;
		}

		if (visibleBoatCheckBox.isSelected()) {//affiche les bateaus en transparent si demande
			addBoat(target, ia, 0.5);
		}

	}

	/**
	 * met a jour la grille de gauche 
	 * @param event
	 * @throws InstantiationException
	 */
	public void updateGrid(EventHandlerBN event) throws InstantiationException {
		boat.getChildren().clear();
		boat.getChildren().add(new ImageView(fond));
		int x = decalage, y = decalage;

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				ImageView iv = new ImageView(Boat.EAU.getImage());
				iv.setTranslateX(x);
				iv.setTranslateY(y);
				boat.getChildren().add(iv);

				if (ia.cible[i][j] == Boat.RATE) {
					ImageView iv2 = new ImageView(ContentManager.loadImage(ContentManager.IBN_RATE));
					iv2.setTranslateX(x);
					iv2.setTranslateY(y);
					boat.getChildren().add(iv2);
				}

				if (event != null) {
					int tmp = i;
					int tmp2 = j;
					iv.setOnMouseClicked(e -> {//ajoute l'event hander passe en parametre si il existe

						event.handle(iv, tmp, tmp2, player.getOrientation());

					});

				}

				y += tailleCase;
			}
			x += tailleCase;
			y = decalage;
		}

		// ajout des bateaux
		addBoat(boat, player, 1);

		// ajout des effets de feu
		addFire(boat, ia.cible);

	}

	
	/**
	 * 
	 * @param group	 groupe dans lequel ajouter les bateaux
	 * @param joueur joueur pour lequel on souhaite placer les bateaux
	 * @param transparence transparence des bateaux
	 * @throws InstantiationException
	 */
	public void addBoat(Group group, BNPlayer joueur, double transparence) throws InstantiationException {
		// ajout des bateaux
		for (int i = 0; i < 6; i++) {
			ImageView iv = new ImageView();
			Couple c = joueur.listeBateau[i];
			if (c != null) {
				iv.setImage(c.type.getImage());
				iv.setOpacity(transparence);
				if(joueur instanceof BNPlayer_IA) {
					iv.setMouseTransparent(true);
				}
				if (c.horizontal) {
					iv.setRotate(90);
					if (c.type.nbCase % 2 != 0) {
						iv.setTranslateX(c.coorX * Boat.tailleCase + Boat.tailleCase * (c.type.nbCase / 2) + decalage);
						iv.setTranslateY(c.coorY * Boat.tailleCase - Boat.tailleCase * (c.type.nbCase / 2) + decalage);
					} else {
						iv.setTranslateX(c.coorX * Boat.tailleCase + Boat.tailleCase * ((c.type.nbCase - 1) / 2)
								+ (Boat.tailleCase / 2) + decalage);
						iv.setTranslateY(c.coorY * Boat.tailleCase - Boat.tailleCase * ((c.type.nbCase - 1) / 2)
								- (Boat.tailleCase / 2) + decalage);
					}
				} else {
					iv.setTranslateX(c.coorX * Boat.tailleCase + decalage);
					iv.setTranslateY(c.coorY * Boat.tailleCase + decalage);
				}

				group.getChildren().add(iv);
			}

		}
	}

	public void addFire(Group group, Boat[][] data) throws InstantiationException {
		// ajout des effets de feu
		int x, y;
		if (data != null) {

			x = y = decalage;
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 10; j++) {
					if (ia.cible[i][j] == Boat.TOUCHE) {
						ImageView iv = new ImageView(ContentManager.loadImage(ContentManager.IBN_FEU));
						iv.setTranslateX(x);
						iv.setTranslateY(y);
						group.getChildren().add(iv);
						// ajout de l'image de feu
					}
					y += tailleCase;

				}
				x += tailleCase;
				y = decalage;
			}
		}
	}

	public void initGrid() {
		update(null);
	}

	/**
	 * augmente le score et met a jour l'affichage
	 */
	public void scoreUp() {

		player.score += 500;
		refreshScore();
		increaseScore.play();
	}

	/**
	 * decremente le score et met a jour l'affichage
	 */
	public void scoreDown() {

		player.score -= 500;
		refreshScore();
		decreaseScore.play();
	}

	/**
	 * met a jour l'affichage du score
	 */
	public void refreshScore() {
		score.setText("Score :" + player.score + "/20000");
	}

}