package Bataille_naval;

import java.util.Random;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import projet4jeuxl3.ContentManager;
import projet4jeuxl3.Score;

/***
 * 
 * @author samuel
 *
 */
public class BNPlayer {
	boolean fin = false;// jeu fini ou non
	static BNPlayer player;// joueur humain
	Boat[][] cible, myGrid;// tableau contenant la disposition des bateaux(myGrid) et la zone de tire
	double MouseX = 0, MouseY = 0;// coordonnees de la souris
	int orientation = 1;// orientation du bateau a placer
	int nbBateau = 0;// nombre de bateau pos�s
	Couple[] listeBateau = new Couple[6];// bateau et leur orientation
	Grille g;// grille qui gere l'affichage
	public boolean myTurn = true;
	BNPlayer_IA ia;// ia que le joueur affronte
	BatailleNavaleStage origin;// stage d'origine
	int vie = 20;// jeu de base 20 point de vie
	int ct = 3, torp = 2, porte = 5, sous1 = 3, sous2 = 3, croiseur = 4;// vie de chaque bateau
	int score = 10000;// score du joueur
	Timeline tlDestroyedBoat;// animation destruction d'un bateau
	MediaPlayer heavyExplosion = new MediaPlayer(
			ContentManager.loadMedia(ContentManager.MBN_HEAVY_EXPLOSION));
	
	/**
	 * envoie le score du joueur a la classe Score
	 */
	public void sendScore() {
		Score.addScore(Score.BN, score);
	}

	/**
	 * diminue les points de vie globaux
	 * 
	 * @param t bateau touch�
	 * @return t detruit ou non
	 */
	public boolean increaseTouche(Boat t) {
		g.scoreDown();
		vie--;
		if (vie == 0) {// si plus de vie, affiche l'ecran de fin de jeu et envoie le score
			fin = true;
			sendScore();
			origin.fin.setScore(score);
			origin.fin.setVictory(false);
			origin.fin.Show();
		}
		return decreaseBoatLife(t,0);

	}

	/**
	 * 
	 * @param t			le bateau touch�
	 * @param joueur	0-> joueur;  1-> ia
	 * @return
	 */
	public boolean decreaseBoatLife(Boat t,int joueur) {
		int vieBateau = 0;
		switch (t) {
		case PORTEAVION:
			porte--;
			vieBateau = porte;
			break;
		case CONTRETORPILLEUR:
			ct--;
			vieBateau = ct;
			break;
		case CROISEUR:
			croiseur--;
			vieBateau = croiseur;
			break;
		case SOUSMARIN:
			sous1--;
			vieBateau = sous1;
			break;
		case SOUSMARIN2:
			sous2--;
			vieBateau = sous2;
			break;
		case TORPILLEUR:
			torp--;
			vieBateau = torp;
			break;
		default:
			break;
		}

		if (vieBateau == 1) {// envoie une alerte au joueur si un bateau n'a plus qu'une vie
			g.Alert();
			g.boatStateChange(t, joueur, 0);
		} else if (vieBateau == 0) {// envoie une alerte au joueur si un bateau est mort
			g.Alert();
			tlDestroyedBoat.play();
			g.boatStateChange(t, joueur, 1);
			return true;
		}
		return false;
	}

	public Grille getGrille() {
		return g;

	}

	public BNPlayer(BNPlayer_IA ia) {
		this.ia = ia;// parametre l'ia
		player = this;
		tlDestroyedBoat = new Timeline();// creer la timeline utilis�e pour l'animation de destruction d'un bateau
		tlDestroyedBoat.getKeyFrames().addAll(new KeyFrame(Duration.ZERO, e -> {
			heavyExplosion.play();
		}), new KeyFrame(Duration.seconds(9), e -> {
			heavyExplosion.stop();
		}));
		cible = new Boat[10][10];// cr�er le tableau cible en initialisant toutes les cases a vide
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				cible[i][j] = Boat.VIDE;
			}
		}
		g = new Grille(this, ia, 70, ContentManager.loadImage(ContentManager.IBN_FOND), Boat.tailleCase);
		this.ia.setGrille(g);
	}

	public BNPlayer() {
		tlDestroyedBoat = new Timeline();
		tlDestroyedBoat.getKeyFrames().addAll(new KeyFrame(Duration.ZERO, e -> {
			heavyExplosion.play();
		}), new KeyFrame(Duration.seconds(9), e -> {
			heavyExplosion.stop();
		}));
		cible = new Boat[10][10];
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				cible[i][j] = Boat.VIDE;
			}
		}

	}

	/**
	 * verifie si un bateau peut etre plac� � un endroit
	 * 
	 * @param b           bateau que l'on souhaite placer
	 * @param i           coordonn�e x
	 * @param j           coordonn�e y
	 * @param orientation orientation du bateau (0 - horizontal ; 1 - vertical)
	 * @return
	 */
	protected boolean checkPlacement(Boat b, int i, int j, int orientation) {
		if (i >= -1 && j >= 0 && j <= 10 && myGrid[i][j] == Boat.EAU) {// verifie que les coordonn�es existe et que la
																		// case correspond � de l'eau
			if (orientation == 0) {
				if (i + b.nbCase <= 10) {// verifie que le bateau ne sort pas de la map
					for (int x = i; x < i + b.nbCase; x++) {// verifie que le bateau n'est pas en collision avec un
															// autre
						if (myGrid[x][j] != Boat.EAU)
							return false;
					}
					return true;
				}

			} else {// meme chose mais � la verticale
				if (j - b.nbCase >= -1) {
					for (int x = j; x > j - b.nbCase; x--) {
						if (myGrid[i][x] != Boat.EAU)
							return false;
					}
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * permet d'initialiser la grille
	 */
	public void initGrid(BatailleNavaleStage origin) throws InstantiationException {

		myGrid = new Boat[10][10];// creer la grille joueur et place de l'eau dans toutes les cases
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				myGrid[i][j] = Boat.EAU;
			}
		}

		initGrid2(origin, Boat.PORTEAVION);// demande � placer un porte avion

	}

	public int getOrientation() {
		return orientation;
	}

	public void setOrientation(int i) {
		orientation = i;
	}

	public int getNbBateau() {
		return nbBateau;
	}

	/**
	 * permet de placer le bateau pass� en parametre
	 * 
	 * @param origin
	 * @param t
	 * @throws InstantiationException
	 */
	public void initGrid2(BatailleNavaleStage origin, Boat t) throws InstantiationException {
		this.origin = origin;
		if (nbBateau < 6) {
			EventHandlerBN event = new EventHandlerBN() {// creer un event handler qui permettra de placer un bateau et
															// de parametrer le prochain bateau a placer

				@Override
				public void handle(ImageView iv, int i, int j, int orientation) {
					if (BNPlayer.player.checkPlacement(t, i, j, orientation)) {
						t.placement(BNPlayer.player.myGrid, i, j, orientation, player.listeBateau);
						BNPlayer.player.setOrientation(1);
						Boat newBoat = null;
						switch (t) {
						case PORTEAVION:
							newBoat = Boat.CROISEUR;
							break;
						case CROISEUR:
							newBoat = Boat.CONTRETORPILLEUR;
							break;
						case CONTRETORPILLEUR:
							newBoat = Boat.SOUSMARIN;
							break;
						case SOUSMARIN:
							newBoat = Boat.SOUSMARIN2;
							break;
						case SOUSMARIN2:
							newBoat = Boat.TORPILLEUR;
						default:
							break;
						}
						try {
							BNPlayer.player.initGrid2(origin, newBoat);
						} catch (InstantiationException e) {
							e.printStackTrace();
						}
					}
				}
			};

			origin.getRoot().getChildren().clear();// vide l'ecran
			g.update(event);// met a jour l'ecran
			Group tab = player.getGrille().board;// ajoute le tableau d'information a l'�cran
			tab.setTranslateX(500);

			origin.getRoot().getChildren().add(tab);

			origin.getRoot().getChildren().add(g.getBoat());

			origin.getScene().setCursor(Cursor.NONE);
			ImageView iv = new ImageView(t.getImage());
			iv.setTranslateX(MouseX - (Boat.tailleCase / 2));
			iv.setTranslateY(MouseY - (Boat.tailleCase * t.nbCase) + (Boat.tailleCase / 2));
			origin.getRoot().getChildren().add(iv);
			origin.getRoot().requestFocus();

			iv.setMouseTransparent(true);

			origin.getRoot().setOnMouseMoved(e -> { // permet de remplacer le curseur de la souris par le bateau �
													// placer
				if (iv.getRotate() == 0) {

					iv.setTranslateX(e.getX() - (Boat.tailleCase / 2));
					iv.setTranslateY(e.getY() - (Boat.tailleCase * t.nbCase) + (Boat.tailleCase / 2));
				} else {

					if (t.nbCase % 2 != 0) {
						iv.setTranslateX(e.getX() + (t.nbCase / 2) * Boat.tailleCase - (Boat.tailleCase / 2));
						iv.setTranslateY(e.getY() - (t.nbCase * (Boat.tailleCase / 2)));
					} else {
						iv.setTranslateX(e.getX() + (Boat.tailleCase * ((t.nbCase - 1) / 2)));
						iv.setTranslateY(e.getY() - (Boat.tailleCase * ((t.nbCase + 1) / 2)));
					}
				}

				MouseX = e.getX();
				MouseY = e.getY();

			});

			origin.getRoot().setOnKeyPressed(e -> {// permet au bateau que l'on souhaite placer de tourner a 90 degres
				if (e.getCode() == KeyCode.R) {
					if (iv.getRotate() == 90) {
						orientation = 1;
						iv.setRotate(0);
						iv.setTranslateX(MouseX - (Boat.tailleCase / 2));
						iv.setTranslateY(MouseY - (Boat.tailleCase * t.nbCase) + (Boat.tailleCase / 2));
					} else {
						orientation = 0;
						iv.setRotate(90);

						if (t.nbCase % 2 != 0) {
							iv.setTranslateX(MouseX + (t.nbCase / 2) * Boat.tailleCase - (Boat.tailleCase / 2));
							iv.setTranslateY(MouseY - (t.nbCase * (Boat.tailleCase / 2)));
						} else {
							iv.setTranslateX(MouseX + (Boat.tailleCase * ((t.nbCase - 1) / 2)));
							iv.setTranslateY(MouseY - (Boat.tailleCase * ((t.nbCase + 1) / 2)));
						}

					}
				}

			});

			nbBateau++;
		} else {// si tous les bateaux ont �taient plac�s on lance le jeu
			origin.getScene().setCursor(Cursor.DEFAULT);
			origin.getRoot().getChildren().clear();
			g.update(null);
			origin.getRoot().getChildren().add(g.getBoat());
			origin.launchGame();
		}

	}

	/**
	 * indique au joueur qu'il peut tirer et verifie si la partie est termin�
	 */
	public void myTurn() {

		g.updateBoard("Pr�t � \nfaire feu", null);

		if (!fin)
			myTurn = true;
	}

	/**
	 * lance le tour de l'ia
	 */
	public void setTurnFalse() {
		myTurn = false;
		ia.myTurn();
	}

	/**
	 * met � jour les point de vie et lance l'animation d'un coup
	 * 
	 * @param t
	 */
	public void touche(Boat t) {
		if (g.playAnimation) {
			if (increaseTouche(t))
				shake(3);
			else
				shake(1);
		} else {
			increaseTouche(t);
		}

		g.update(null);

	}

	/**
	 * permet de secouer l'ecran
	 * 
	 * @param number nombre de fois ou l'action est repete
	 */
	public void shake(int number) {
		Timeline tl = new Timeline();
		Random rdm = new Random();

		tl.getKeyFrames().addAll(new KeyFrame(Duration.ZERO, new KeyValue(origin.getRoot().translateXProperty(), 0)),
				new KeyFrame(Duration.ZERO, new KeyValue(origin.getRoot().translateYProperty(), 0)),
				new KeyFrame(Duration.millis(50),
						new KeyValue(origin.getRoot().translateXProperty(), (rdm.nextDouble() - 0.5) * 10)),
				new KeyFrame(Duration.millis(50),
						new KeyValue(origin.getRoot().translateYProperty(), (rdm.nextDouble() - 0.5) * 10)));
		tl.setAutoReverse(true);
		tl.setCycleCount(20 * number);
		tl.play();

	}

}
