package Sudoku;

import java.util.ArrayList;

/**
 * 
 * @author Erwann
 *
 */

public class Sudoku {
	public int[][] grille;
	
	public Sudoku() {
		grille = new int[9][9];
		for (int i = 0 ; i<9 ; i++)
		 {
			 for(int y = 0 ; y < 9 ; y++) 
			 {
				 grille[i][y] = 0 ;
			 }
		 }
		//grilleJeuGen();
		//Pour la demo
		int grille2[][] ={ 
		 {1,0,0,0,0,0,0,0,6},//2
		 {0,0,6,0,2,0,7,0,0},//3
		 {7,8,9,4,5,0,1,0,3},//7
		 {0,0,0,8,0,7,0,0,4},//3
		 {0,0,0,0,3,0,0,0,0},//1
		 {0,9,0,0,0,4,2,0,1},//4
		 {3,1,2,9,7,0,0,4,0},//6
		 {0,4,0,0,1,2,0,7,8},//5
		 {9,0,8,0,0,0,0,0,0} //2
		};
		grille = grille2;
		
		//grilleJeuGen();
				  //{1,2,3,4,5,6,7,8,9}	
		
	}


	
	public void grilleJeuGen() {
		int max = 8;
		int min = 0;
		int nbE = 33; // nb de chiffre � placer au d�part
		int t=0;
		
		for (int i = 0 ; i < nbE; i++) {
			//s�lection d'une case 
			int x = min + (int)(Math.random() * ((max - min) + 1));
			int y = min + (int)(Math.random() * ((max - min) + 1));
				
			//Si la place est d�ja occup�
				while(grille[x][y] != 0) {
					x = min + (int)(Math.random() * ((max - min) + 1));
					y = min + (int)(Math.random() * ((max - min) + 1));
				}
				
				// Attribution du valeur
			 grille[x][y] = 1 + (int)(Math.random() * ((9 - 1) + 1));
			while(!valide() && t<5) {
				grille[x][y] = 1 + (int)(Math.random() * ((9 - 1) + 1));
				t++;
				if(!valide()) {grille[x][y] = 0;}
			}
			t=0;
		}
	}
	
	public void afficher() {
		String s;
		 for (int i = 0;i<9;i++)
		 {
			 s="";
			 for(int y = 0; y<9;y++) 
			 {
				s=s+" "+"|"+" "+grille[i][y];
				if (y > 0)
					{
					if(y % 3 == 2)
						{
							s=s+"|";
						}
					}
			 }
			 s=s+" "+"|";
			 System.out.println(s);
			 if (i > 0)
				{
				if(i % 3 == 2)
					{
					 System.out.println();
					}
				}
			
		 }
	}
	
	public void ajouter(int i, int y,int val)
	{
		assert val > 0 && val< 9;
		assert i > 0 && i < 9;
		assert y > 0 && y < 9;
		
		grille[i][y] = val;
	}
	
	public int getV(int x, int y){
		return grille[x][y];
	}
	
	public boolean valide()
	{
		 
		 return verificationCarre() & verificationLigne() & verificationColonne();
	}
	
	public boolean gagnante() {
		int x=0;
		int y=0;
		while(x <9) {
			y=0;
			while(y<9) {
				
				if(grille[x][y]==0) {return false;}y++;
			}
			x++;
		}
		
		
		return valide();
	}
	
	public boolean verificationCarre(){
	
			int z,t=0;
			ArrayList<Integer> Lis = new ArrayList<Integer>();
			while(t<3)
			{	z=0;
				while(z<3) {
					Lis = new ArrayList<Integer>();
					for(int i=0+3*t;i<3+3*t;i++)
					{
						for(int y = 0+3*z;y<3+3*z;y++)
						{
							int a = grille[i][y];
							if(a!=0) {
								if(Lis.contains(a)){return false;}
								Lis.add(a);
							}
						}
					}
					z++;
				}
				t++;
			}
		return true;
	}
	
	
	public boolean verificationLigne() {
	
		int i=0,y=0;
		ArrayList<Integer> Lis = new ArrayList<Integer>();
		while(i<9)
		{
			Lis = new ArrayList<Integer>();
			y=0;
			while(y<9)
			{
				int a = grille[i][y];
				if(a!=0) {
					if(Lis.contains(a)){return false;}
					Lis.add(a);
				}
				y++;
			}
			i++;
		}		
		return true;
	}
	
	public boolean verificationColonne() {
		int i=0, y=0;
		ArrayList<Integer> Lis = new ArrayList<Integer>();
		while(i<9)
		{
			Lis = new ArrayList<Integer>();
			y=0;
			while(y<9)
			{
				int a = grille[y][i];
				if(a != 0) {
					if(Lis.contains(a)){return false;}
					Lis.add(a);
				}
				y++;
			}
			i++;
		}		
		return true;
	}
	
}
