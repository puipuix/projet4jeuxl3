package Sudoku;

import javafx.scene.control.Label;
/**
 * 
 * @author Erwann
 *
 */
public class Case extends Label{
	//case d'une grille de sudoku
	public int x;
	public int y;
	public boolean lock; // appliquer au case par d�faut
	
	public Case(int x, int y){
		super();
		this.x = x;
		this.y = y;
		lock = false;
	}
	/**
	 * 
	 * verrouille la case ce qui empeche sa modification
	 *
	 */
	public void lock() {
		lock = true;
	}
}
