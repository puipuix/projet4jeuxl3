package Sudoku;

import java.time.LocalTime;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import projet4jeuxl3.ContentManager;
/**
 * 
 * @author Erwann
 *
 */
public class Timer extends Group{
	private Rectangle recEx = new Rectangle();
	private Rectangle recIn = new Rectangle();
	
	private Circle circle = new Circle();
	private Circle circle2 = new Circle();
	
	private boolean allumer = true;
	
	private Label min = new Label();
	private Label sec = new Label();
	private Label deuxpoints = new Label(":");
	private LocalTime lt = LocalTime.of(0, 0, 0);
	private Timeline tls;
	private int minute;
	
	public Timer() {
		// gestion de la postion des elements ainsi que de leur couleur et format
		recEx.setHeight(50);
		recEx.setWidth(100);
		recEx.setFill(Color.BLACK);
		
		recIn.setHeight(45);
		recIn.setWidth(95);
		recIn.setFill(Color.WHITE);
		
		recIn.setTranslateX(2);
		recIn.setTranslateY(2);
		
		circle.setRadius(3);
		circle.setFill(Color.BLACK);
		circle2.setRadius(2);
		circle2.setFill(Color.RED);
		
		circle.setTranslateX(90.00);
		circle2.setTranslateX(90.00);
		
		circle.setTranslateY(9.00);
		circle2.setTranslateY(9.00);
		
		deuxpoints.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL,17));
		
		min.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL,17));
		sec.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL,17));
		
		sec.setTranslateX(55.00);
		sec.setTranslateY(12.00);
		
		min.setTranslateX(10.00);
		min.setTranslateY(12.00);
		
		tls = new Timeline();
		// g�re l'incr�mentation au niveau du temps
		tls.getKeyFrames().add(new KeyFrame(Duration.seconds(1), e -> {
			lt=lt.plusSeconds(1);
			minute = lt.getMinute();
			if(minute<10)min.setText("0"+minute);
			else min.setText(""+minute);
			int seconde = lt.getSecond();
			if (seconde<10)sec.setText("0"+seconde);
			else sec.setText(""+seconde);
			clignote();
		}));
		tls.setCycleCount(Timeline.INDEFINITE);
		tls.play();
		this.getChildren().addAll(recEx,recIn,circle,circle2,min,sec);
	}
	// inverse l'etat de la lumiere en haut en droite
	private void clignote() {if(allumer) {circle2.setFill(Color.GREY);}else{circle2.setFill(Color.RED);}
	allumer = !allumer;}
}
