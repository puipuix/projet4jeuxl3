package Sudoku;

import java.time.LocalTime;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import projet4jeuxl3.ContentManager;
import projet4jeuxl3.MultipleRoot;
import projet4jeuxl3.PauseMenuJeux;
import projet4jeuxl3.PauseMenuVictoireDefaite;
import projet4jeuxl3.Score;

/**
 * 
 * @author Erwann
 *
 */
public class SudoAff extends Stage {

	public Sudoku sudo = new Sudoku();
	public Case lbsel = new Case(0, 0);
	public LocalTime lt = LocalTime.of(0, 0, 0);
	public int score = 10000;

	public Label fondGrille = new Label();
	public Label lScore = new Label();
	public Timer tim = new Timer();
	public ImageView iW = new ImageView(new Image(SudoAff.class.getResourceAsStream("/pb.png")));

	public SudoAff() {
		this.setResizable(false);
		tim.setLayoutX(500);
		tim.setLayoutY(20);

		iW.setScaleX(2);
		iW.setScaleY(1.5);
		
		iW.setLayoutX(520);
		iW.setLayoutY(200);
		
		/**
		 * 
		 * genere le fond derri�re la grille
		 *
		 */
		fondGrille.setBackground(new Background(new BackgroundFill(Color.WHITE, null, Insets.EMPTY)));
		fondGrille.setMinHeight(450);
		fondGrille.setMinWidth(450);

		MultipleRoot multipleRoot = new MultipleRoot();
		
		/**
		 * 
		 * menue de fin
		 *
		 */
		PauseMenuVictoireDefaite menuFin = new PauseMenuVictoireDefaite(this, () -> {
			return new SudoAff();
		});
		
		PauseMenuJeux menuPause = new PauseMenuJeux(this);

		Group root = new Group();
		Scene sc = new Scene(multipleRoot, 650, 441, Color.rgb(150, 150, 150));

		
		StackPane sp = new StackPane(root);
		sp.setAlignment(Pos.CENTER);
		multipleRoot.setMainRoot(sp);
		multipleRoot.getSubRoots().addAll(menuFin, menuPause);

		this.setScene(sc);
		this.setTitle("Sudoku");

		lScore.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 19));
		lScore.setLayoutX(450);
		lScore.setLayoutY(80);
		lScore.setText("Score : " + score);
		
		// gestion du score perte de point toutes les minuttes
		Timeline tl2 = new Timeline();
		tl2.getKeyFrames().add(new KeyFrame(Duration.minutes(1), e -> {
			score -= 60;
			lScore.setText("Score : " + score);
		}));

		tl2.setCycleCount(Timeline.INDEFINITE);
		tl2.play();
		
		// Validation de la grille
		Button val = new Button();
		val.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 18));
		val.setLayoutX(493);
		val.setLayoutY(260);
		val.setText("Valider");
		val.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (sudo.gagnante()) {
					//declare la victoire du joueur
					Score.addScore(Score.SUDOKU, score);
					menuFin.setScore(score);
					menuFin.setSubTitle("Grille valide !");
					menuFin.Show();
				} else {
					//System.out.println("Grille non termine");
				}
			}
		});

		Button menu = new Button();
		menu.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 18));
		menu.setLayoutX(510);
		menu.setLayoutY(200);
		menu.setText("Menu");
		menu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				menuPause.Show();
			}
		});

		// Gridpane avec 9 sous gridpane (grille de Sudoku)
		GridPane gridpane = new GridPane();
		for (int i = 0; i < 3; i++) {
			for (int i2 = 0; i2 < 3; i2++) {
				// Generation des sousgridpanes
				GridPane sousgridpane = new GridPane();
				// Generation des cases
				for (int y = 0; y < 3; y++) {
					for (int z = 0; z < 3; z++) {
						int xi = i * 3 + y, yi = i2 * 3 + z;
						Case lb = new Case(xi, yi);
						int v = sudo.getV(xi, yi);

						if (v != 0) {
							lb.setText("" + v);
							lb.setTextFill(Color.BLACK);
							lb.lock();
						}
						lb.setMinSize(50, 50);
						lb.setMaxSize(50, 50);
						lb.setAlignment(Pos.CENTER);
						lb.setFont(ContentManager.LoadFont(ContentManager.FONT_PIXEL, 17));

						// Active la case lorsque l'on appuie dessus
						lb.setOnMouseClicked(e -> {
									// changement de couleur de l'ancienne(deselection)
							lbsel.setBackground(
									new Background(new BackgroundFill(Color.TRANSPARENT, null, Insets.EMPTY)));
							lbsel.setTextFill(Color.BLUE);
							if (!lb.lock) {
								lbsel = lb;
							}
							//selectionne la nouvelle case
							lbsel.setTextFill(Color.DARKBLUE);
							lbsel.setBackground(
									new Background(new BackgroundFill(Color.LIGHTGREY, null, Insets.EMPTY)));
						});
						sousgridpane.add(lb, y, z);
						sousgridpane.setGridLinesVisible(true);
					}
				}
				gridpane.add(sousgridpane, i, i2);
			}
		}
		
		gridpane.setGridLinesVisible(true);
		//ajout des enfants dans le root
		root.getChildren().add(iW);
		root.getChildren().add(fondGrille);
		root.getChildren().add(gridpane);
		root.getChildren().add(lScore);
		root.getChildren().add(tim);

		// Recuperer les touches/chiffres entrer 123456789 & supr
		root.requestFocus();
		root.setOnKeyPressed(e -> {
			int v = 0;
			switch (e.getCode()) {
			case NUMPAD1:
				v = 1;
				break;
			case NUMPAD2:
				v = 2;
				break;
			case NUMPAD3:
				v = 3;
				break;
			case NUMPAD4:
				v = 4;
				break;
			case NUMPAD5:
				v = 5;
				break;
			case NUMPAD6:
				v = 6;
				break;
			case NUMPAD7:
				v = 7;
				break;
			case NUMPAD8:
				v = 8;
				break;
			case NUMPAD9:
				v = 9;
				break;
			case BACK_SPACE:
				v = -1;
				break;

			default:
				break;
			}
			if (v > 0) {
				// g�re les nombre entrer puis change le label s�lectionner et l'emplacement correspondant dans la matrice
				lbsel.setText("" + v);
				int tmp = sudo.grille[lbsel.x][lbsel.y];
				sudo.grille[lbsel.x][lbsel.y] = v;

				if (!sudo.valide()) {
					if (tmp != 0) {
						lbsel.setText("" + tmp);
						sudo.grille[lbsel.x][lbsel.y] = tmp;
					} else {
						lbsel.setText("");
					}
					sudo.grille[lbsel.x][lbsel.y] = tmp;

				} else {
					if (tmp != 0 && tmp != v)
						score -= 5;
					lScore.setText("Score : " + score);
				}

			}
			if (v == -1) {
				lbsel.setText("");
				sudo.grille[lbsel.x][lbsel.y] = 0;
			}
		});

		root.getChildren().add(val);
		root.getChildren().add(menu);
		this.setScene(sc);
		//gere le changement de taille de la feunetre
		ChangeListener<Number> resize = new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (observable.equals(heightProperty())) {
					setMinHeight(getHeight());
					heightProperty().removeListener(this);
				} else {
					setMinWidth(getWidth());
					widthProperty().removeListener(this);
				}
				multipleRoot.resizeMainRoot(true);
			}
		};
		this.heightProperty().addListener(resize);
		this.widthProperty().addListener(resize);
	}

}
